<?php
namespace moocApp\MoocBundle\Entity;

class Mail {
private $nom;
private $prenom;
private $tel;
private $from;
private $text;
private $adresse;
private $nomDeLaSociete;

function getadresse() {
    return $this->adresse;
}
function getnomDeLaSociete() {
    return $this->nomDeLaSociete;
}

function getNom() {
    return $this->nom;
}

function getPrenom() {
    return $this->prenom;
}

function getTel() {
    return $this->tel;
}

function getFrom() {
    return $this->from;
}

function getText() {
    return $this->text;
}
function setadresse($adresse) {
    $this->adresse = $adresse;
}

function setNom($nom) {
    $this->nom = $nom;
}

function setnomDeLaSociete($nomDeLaSociete) {
    $this->nomDeLaSociete = $nomDeLaSociete;
}

function setPrenom($prenom) {
    $this->prenom = $prenom;
}

function setTel($tel) {
    $this->tel = $tel;
}

function setFrom($from) {
    $this->from = $from;
}

function setText($text) {
    $this->text = $text;
}
}