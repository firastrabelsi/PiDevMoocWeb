<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace moocApp\MoocBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**

 * @ORM\Entity
 * @ORM\Table(name="invitation")
 * @ORM\Entity
 */
class Invitation {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     */
    private $source;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     */
    private $destination;

    /**
     * @ORM\Column(type="date")
     */
    private $dateinvitation;

    /**
     *
     * @ORM\Column( type="string", length=255)
     */
    private $etat;
    

    
    function getId() {
        return $this->id;
    }

    function getSource() {
        return $this->source;
    }

    function getDestination() {
        return $this->destination;
    }

    function getDateinvitation() {
        return $this->dateinvitation;
    }

    function getEtat() {
        return $this->etat;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setSource($source) {
        $this->source = $source;
    }

    function setDestination($destination) {
        $this->destination = $destination;
    }

    function setDateinvitation($dateinvitation) {
        $this->dateinvitation = $dateinvitation;
    }

    function setEtat($etat) {
        $this->etat = $etat;
    }

      

}
