<?php

namespace moocApp\MoocBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**

 * @ORM\Entity
 * @ORM\Table(name="utilisateur")
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="moocApp\MoocBundle\Entity\UserRepository")
 */
class User extends BaseUser {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255 , nullable=true)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255 , nullable=true)
     */
    private $prenom;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255)
     */
    private $url = "";

    /**
     * @var string
     *
     * @ORM\Column(name="etat", type="string", length=255)
     */
    private $etat = "";

    /**
     *
     * @ORM\Column( type="string", length=255)
     */
    private $adresse;

    

    /**
     *
     * @ORM\Column( type="string", length=255)
     */
    private $site;

    /**
     *
     * @ORM\Column( type="string", length=255)
     */
    private $certificat;

    /**
     *
     * @ORM\Column( type="string", length=255 , nullable=true)
     */
    private $nomDeLaSociete;

    /**
     *
     * @ORM\Column( type="float")
     */
    private $num;

    /**
     *
     * @ORM\Column( type="string", length=255 , nullable=true)
     */
    private $specialite;
    function getSpecialite() {
        return $this->specialite;
    }

    function getEtat_c() {
        return $this->etat_c;
    }

    function setSpecialite($specialite) {
        $this->specialite = $specialite;
    }

    function setEtat_c($etat_c) {
        $this->etat_c = $etat_c;
    }

        /**
     *
     * @ORM\Column( type="integer", length=255 , nullable=true)
     */
    private $etat_c;

    function getNum() {
        return $this->num;
    }

    function getSite() {
        return $this->site;
    }

    function getCertificat() {
        return $this->certificat;
    }

    function setNum($num) {
        $this->num = $num;
    }

    function setSite($site) {
        $this->site = $site;
    }

    function setCertificat($certificat) {
        $this->certificat = $certificat;
    }

    function getId() {
        return $this->id;
    }

    function getUrl() {
        return $this->url;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setUrl($url) {
        $this->url = $url;
    }

    function getEtat() {
        return $this->etat;
    }

    function setEtat($etat) {
        $this->etat = $etat;
    }

    function getNom() {
        return $this->nom;
    }

    function getprenom() {
        return $this->prenom;
    }

    function setNOM($nom) {
        $this->nom = $nom;
    }

    function setpreNOM($prenom) {
        $this->prenom = $prenom;
    }

    function getAdresse() {
        return $this->adresse;
    }

    function getNomDeLaSociete() {
        return $this->nomDeLaSociete;
    }

    function setAdresse($adresse) {
        $this->adresse = $adresse;
    }

    function setNomDeLaSociete($nomDeLaSociete) {
        $this->nomDeLaSociete = $nomDeLaSociete;
    }

    public function __construct() {
        parent::__construct();
// your own logic
    }
    
}
