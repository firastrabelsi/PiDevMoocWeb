<?php

namespace moocApp\MoocBundle\Redirection;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;

class AfterLoginRedirection implements AuthenticationSuccessHandlerInterface {
    /*
     *
     * @var \Symfony\Component\Routing\RouterInterface    
     */

    private $router;

    /**
     * @param RouterInterface $router    
     */
    public function __construct(RouterInterface $router) {
        $this->router = $router;
    }

    /**
     * @param Request $request  
     * @param TokenInterface $token    
     * @return RedirectResponse    
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token) {
// Get list of roles for current user      
        $roles = $token->getRoles();
// Tranform this list in array       
        $rolesTab = array_map(function($role) {
            return $role->getRole();
        }, $roles);

// S'il s'agit d'un admin ou d'un super admin on le redirige vers le backoffice
        $user = $token->getUser();

        if (in_array('ROLE_SUPER_ADMIN', $rolesTab, true)) {
            $redirection = new RedirectResponse($this->router->generate('dashboard'));
        } elseif (in_array('ROLE_APPRENANT', $rolesTab, true)) {
            $redirection = new RedirectResponse($this->router->generate('pidevmooc_profil'));
        } elseif (in_array('ROLE_ORGANISME', $rolesTab, true)) {
            if ($user->getEtat()== "valide a terminer") {
                return $redirection = new RedirectResponse($this->router->generate('registerformstep3org'));
            } elseif ($user->getEtat()== "valide") {
                return $redirection = new RedirectResponse($this->router->generate('pidevmooc_homepage'));
            } else {
                return $redirection = new RedirectResponse($this->router->generate('registerformstep3org'));
            }
        } else {
            $redirection = new RedirectResponse($this->router->generate('dashboard'));
        }

        return $redirection;
    }

}
