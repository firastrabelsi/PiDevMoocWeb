<?php

namespace MOOC\commentaireBundle\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping  as ORM;

/**
 * @ORM\Entity
 */
class Cours
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $titre;

    /**
     * @var string
     */
    private $description;

    /**
     * @var \DateTime
     */
    private $datedebut;

    /**
     * @var \DateTime
     */
    private $datefin;

    /**
     * @var string
     */
    private $difficulte;

    /**
     * @var integer
     */
    private $testfinaleId;
   /**
    * @var float
    *@ORM\Column(name="note",type="float")
    */
    private $note=0;
    /**
    *@ORM\Column(type="integer")
    */
    private $nbreV;
    function getNote() {
        return $this->note;
    }

    function getNbreV() {
        return $this->nbreV;
    }

    function setNote($note) {
        $this->note = $note;
    }

    function setNbreV($nbreV) {
        $this->nbreV = $nbreV;
    }

        /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre
     *
     * @param string $titre
     * @return Cours
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string 
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Cours
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set datedebut
     *
     * @param \DateTime $datedebut
     * @return Cours
     */
    public function setDatedebut($datedebut)
    {
        $this->datedebut = $datedebut;

        return $this;
    }

    /**
     * Get datedebut
     *
     * @return \DateTime 
     */
    public function getDatedebut()
    {
        return $this->datedebut;
    }

    /**
     * Set datefin
     *
     * @param \DateTime $datefin
     * @return Cours
     */
    public function setDatefin($datefin)
    {
        $this->datefin = $datefin;

        return $this;
    }

    /**
     * Get datefin
     *
     * @return \DateTime 
     */
    public function getDatefin()
    {
        return $this->datefin;
    }

    /**
     * Set difficulte
     *
     * @param string $difficulte
     * @return Cours
     */
    public function setDifficulte($difficulte)
    {
        $this->difficulte = $difficulte;

        return $this;
    }

    /**
     * Get difficulte
     *
     * @return string 
     */
    public function getDifficulte()
    {
        return $this->difficulte;
    }

    /**
     * Set testfinaleId
     *
     * @param integer $testfinaleId
     * @return Cours
     */
    public function setTestfinaleId($testfinaleId)
    {
        $this->testfinaleId = $testfinaleId;

        return $this;
    }

    /**
     * Get testfinaleId
     *
     * @return integer 
     */
    public function getTestfinaleId()
    {
        return $this->testfinaleId;
    }
}