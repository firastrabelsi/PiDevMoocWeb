<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace MOOC\commentaireBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;



/**
 * @ORM\Entity
 */
class Badge {

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
private $id; 
/** 
 * 
 * 
 * @ORM\Column(name="b1", type="blob", nullable=false) 
 */ 
private $b1; 
/** 
 * 
 * 
 * @ORM\Column(name="b2", type="blob", nullable=false) 
 */ 
private $b2; 
/** 
 * 
 * 
 * @ORM\Column(name="b3", type="blob", nullable=false) 
 */ 
private $b3; 
 /** 
 * 
 * @Assert\File(maxSize="6000000") 
 * 
 */
private $file1;
 /** 
 * 
 * @Assert\File(maxSize="6000000") 
 * 
 */
private $file2;
 /** 
 * 
 * @Assert\File(maxSize="6000000") 
 * 
 */
private $file3;
 /**
 * @ORM\ManyToOne(targetEntity="Cours")
 */
private $cours;
    
function getCours() {
    return $this->cours;
}

function setCours($cours) {
    $this->cours = $cours;
}


function getFile1() {
    return $this->file1;
}

function getFile2() {
    return $this->file2;
}

function getFile3() {
    return $this->file3;
}

function setFile1($file1) {
    $this->file1 = $file1;
}

function setFile2($file2) {
    $this->file2 = $file2;
}

function setFile3($file3) {
    $this->file3 = $file3;
}

function getId() {
    return $this->id;
}

function getB1() {
    return $this->b1;
}

function getB2() {
    return $this->b2;
}

function getB3() {
    return $this->b3;
}



function setId($id) {
    $this->id = $id;
}

function setB1($b1) {
    $this->b1 = $b1;
}

function setB2($b2) {
    $this->b2 = $b2;
}

function setB3($b3) {
    $this->b3 = $b3;
}




}
