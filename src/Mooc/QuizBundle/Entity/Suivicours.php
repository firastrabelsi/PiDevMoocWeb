<?php

namespace Mooc\QuizBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Suivicours
 */
class Suivicours
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $coursId;

    /**
     * @var integer
     */
    private $apprenantId;

    /**
     * @var string
     */
    private $evaluation;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set coursId
     *
     * @param integer $coursId
     * @return Suivicours
     */
    public function setCoursId($coursId)
    {
        $this->coursId = $coursId;

        return $this;
    }

    /**
     * Get coursId
     *
     * @return integer 
     */
    public function getCoursId()
    {
        return $this->coursId;
    }

    /**
     * Set apprenantId
     *
     * @param integer $apprenantId
     * @return Suivicours
     */
    public function setApprenantId($apprenantId)
    {
        $this->apprenantId = $apprenantId;

        return $this;
    }

    /**
     * Get apprenantId
     *
     * @return integer 
     */
    public function getApprenantId()
    {
        return $this->apprenantId;
    }

    /**
     * Set evaluation
     *
     * @param string $evaluation
     * @return Suivicours
     */
    public function setEvaluation($evaluation)
    {
        $this->evaluation = $evaluation;

        return $this;
    }

    /**
     * Get evaluation
     *
     * @return string 
     */
    public function getEvaluation()
    {
        return $this->evaluation;
    }
}
