<?php

namespace Mooc\QuizBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Chapitre
 */
class Chapitre
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $coursId;

    /**
     * @var string
     */
    private $nom;

    /**
     * @var string
     */
    private $description;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set coursId
     *
     * @param integer $coursId
     * @return Chapitre
     */
    public function setCoursId($coursId)
    {
        $this->coursId = $coursId;

        return $this;
    }

    /**
     * Get coursId
     *
     * @return integer 
     */
    public function getCoursId()
    {
        return $this->coursId;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Chapitre
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Chapitre
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }
}
