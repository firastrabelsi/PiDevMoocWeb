<?php

namespace Mooc\QuizBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Invitation
 */
class Invitation
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $sourceId;

    /**
     * @var integer
     */
    private $destinationId;

    /**
     * @var string
     */
    private $dateinvitation;

    /**
     * @var string
     */
    private $etat;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sourceId
     *
     * @param integer $sourceId
     * @return Invitation
     */
    public function setSourceId($sourceId)
    {
        $this->sourceId = $sourceId;

        return $this;
    }

    /**
     * Get sourceId
     *
     * @return integer 
     */
    public function getSourceId()
    {
        return $this->sourceId;
    }

    /**
     * Set destinationId
     *
     * @param integer $destinationId
     * @return Invitation
     */
    public function setDestinationId($destinationId)
    {
        $this->destinationId = $destinationId;

        return $this;
    }

    /**
     * Get destinationId
     *
     * @return integer 
     */
    public function getDestinationId()
    {
        return $this->destinationId;
    }

    /**
     * Set dateinvitation
     *
     * @param string $dateinvitation
     * @return Invitation
     */
    public function setDateinvitation($dateinvitation)
    {
        $this->dateinvitation = $dateinvitation;

        return $this;
    }

    /**
     * Get dateinvitation
     *
     * @return string 
     */
    public function getDateinvitation()
    {
        return $this->dateinvitation;
    }

    /**
     * Set etat
     *
     * @param string $etat
     * @return Invitation
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;

        return $this;
    }

    /**
     * Get etat
     *
     * @return string 
     */
    public function getEtat()
    {
        return $this->etat;
    }
}
