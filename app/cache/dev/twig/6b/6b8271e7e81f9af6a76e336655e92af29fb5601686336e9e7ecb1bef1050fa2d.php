<?php

/* MoocBundle:user:dashboard.html.twig */
class __TwigTemplate_3d568440b26d2a50ec291339de2fa847e2d73fcae2d32219f5be223eb5ceb1d4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'affichage' => array($this, 'block_affichage'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "

<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.1//EN\"
    \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\"> 
<html dir=\"ltr\" lang=\"en-US\" xmlns=\"http://www.w3.org/1999/xhtml\">

\t
<!-- Mirrored from www.nicolaegabriel.info/live/admin-pure/ by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 20 Feb 2016 22:21:15 GMT -->
<head>
\t\t<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />
\t\t<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge,chrome=1\" />

\t\t<title>HTML - Admin Pure</title>
\t\t
\t\t<link rel='stylesheet' href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("publicAdmin/_layout/scripts/jquery.fullcalendar/fullcalendar.css"), "html", null, true);
        echo "\" type='text/css' media='screen' />
\t\t<link rel='stylesheet' href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("publicAdmin/_layout/scripts/jquery.fullcalendar/fullcalendar.print.css"), "html", null, true);
        echo "\" type='text/css' media='print' />
\t\t
\t\t<!-- Styles -->
\t\t<link rel='stylesheet' href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("publicAdmin/_layout/style.css"), "html", null, true);
        echo "\" type='text/css' media='all' />
\t\t
\t\t<!--[if IE]>
\t\t
\t\t\t<link rel='stylesheet' href=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("publicAdmin/_layout/IE.css"), "html", null, true);
        echo "\" type='text/css' media='all'')}}\" />\t\t
\t\t\t
\t\t<![endif]-->
\t\t
\t\t<!-- Fonts -->
\t\t<link href=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("http://fonts.googleapis.com/css?family=Droid+Sans:regular,bold|PT+Sans+Narrow:regular,bold|Droid+Serif:i&amp;v1"), "html", null, true);
        echo "\" rel='stylesheet' type='text/css' />
\t\t
\t\t<script type='text/javascript' src='../../../ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min97e1.js?ver=1.7'></script>
\t\t<script type=\"\" src=\"../../../ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/jquery-ui.min.js\"></script>
\t\t
\t\t<!-- Calendar -->
\t\t<script type='text/javascript' src='publicAdmin/_layout/scripts/jquery.fullcalendar/fullcalendar.min.js'></script>
\t\t
\t\t<!-- Scripts -->
\t\t<script type='text/javascript' src='publicAdmin/_layout/custom.js'></script>
\t\t
\t</head>  
  
\t<body>
\t 
\t\t<div id=\"layout\">
\t\t\t<div id=\"header-wrapper\">
\t\t\t\t<div id=\"header\">
\t\t\t\t\t<div id=\"user-wrapper\" class=\"fixed\">
\t\t\t\t\t\t<div class=\"color-scheme\">
\t\t\t\t\t\t\t<a href=\"#\" class=\"button\">Dropdown suggestion</a>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"user\">
\t\t\t\t\t\t\t<span>Welcome <a href=\"#\">Ghada !</a></span>
\t\t\t\t\t\t\t<span class=\"logout\"><a href=\"http://localhost/moocApp/web/app_dev.php/logout\">Logout</a></span>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t
\t\t\t\t\t<div id=\"launcher-wrapper\" class=\"fixed\">
\t\t\t\t\t\t<div class=\"logo\">
\t\t\t\t\t\t\t<a href=\"";
        // line 58
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("index-2.html"), "html", null, true);
        echo "\"><img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("publicAdmin/_content/back-logo.png"), "html", null, true);
        echo "\" alt=\"\" /></a>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t
\t\t\t\t\t\t<div class=\"launcher\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t
\t\t\t<div class=\"page fixed\">
\t\t\t\t<div id=\"sidebar\">
\t\t\t\t\t<ul id=\"navigation\">
\t\t\t\t\t\t<li class=\"first active\">
\t\t\t\t\t\t\t<div><a href=\"";
        // line 72
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("index-2.html"), "html", null, true);
        echo "\">Dashboard</a><span class=\"icon-nav dashboard\"></span></div>
\t\t\t\t\t\t\t<div class=\"back\"></div>
\t\t\t\t\t\t</li>
\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t<div><a href=\"";
        // line 76
        echo $this->env->getExtension('routing')->getPath("valider_organisme");
        echo "\">Valider organisme</a><span class=\"icon-nav tables\"></span></div>
\t\t\t\t\t\t\t<div class=\"back\"></div>
\t\t\t\t\t\t</li>
\t\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t\t\t\t<li class=\"last\">
\t\t\t\t\t\t\t<div><a href=\"";
        // line 82
        echo $this->env->getExtension('routing')->getPath("afficher_compte_admin");
        echo "\">Liste des comptes</a><span class=\"icon-nav users\"></span></div>
\t\t\t\t\t\t\t<div class=\"back\"></div>
\t\t\t\t\t\t</li>
\t\t\t\t\t</ul>
\t\t\t\t</div>
\t\t\t\t
\t\t\t\t
\t\t\t\t<div id=\"content\">
\t\t\t\t\t<div class=\"fixed index-large-icon\">
\t\t\t\t\t\t<a href=\"#\" class=\"large-icon one\"><span></span></a>
\t\t\t\t\t\t<a href=\"#\" class=\"large-icon two\"><span></span></a>
\t\t\t\t\t\t<a href=\"#\" class=\"large-icon three\"><span></span></a>
\t\t\t\t\t\t<a href=\"#\" class=\"large-icon four\"><span></span></a>
\t\t\t\t\t\t<a href=\"";
        // line 95
        echo $this->env->getExtension('routing')->getPath("afficher_compte_admin");
        echo "\" class=\"large-icon five\"><span></span></a>
\t\t\t\t\t\t<a href=\"#\" class=\"large-icon six\"><span></span></a>
\t\t\t\t\t\t<a href=\"#\" class=\"large-icon seven last\"><span></span></a>
\t\t\t\t\t\t<a href=\"#\" class=\"large-icon ten\"><span></span></a>
\t\t\t\t\t\t<a href=\"#\" class=\"large-icon eleven\"><span></span></a>
\t\t\t\t\t\t<a href=\"#\" class=\"large-icon twelve\"><span></span></a>
\t\t\t\t\t\t<a href=\"#\" class=\"large-icon thirteen\"><span></span></a>
\t\t\t\t\t\t<a href=\"#\" class=\"large-icon fourteen\"><span></span></a>
\t\t\t\t\t\t<a href=\"#\" class=\"large-icon fifteen\"><span></span></a>
\t\t\t\t\t\t<a href=\"#\" class=\"large-icon sixteen last\"><span></span></a>
\t\t\t\t\t</div>
\t\t\t\t\t
\t\t\t\t\t<div class=\"notice-one\">Hi there! I’m just a warning.
\t\t\t\t\t\t<span></span>
\t\t\t\t\t</div>
\t\t\t\t\t
\t\t\t\t\t
\t\t\t\t\t
\t\t\t\t
\t\t\t\t\t<div class=\"hr\"></div>  
\t\t\t\t\t
\t\t\t\t\t<div class=\"fixed\">
\t\t\t\t\t\t<div class=\"col-310\">
\t\t\t\t\t\t\t<div class=\"panel\">
                                                            
                                                             ";
        // line 120
        $this->displayBlock('affichage', $context, $blocks);
        // line 122
        echo "\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t
\t\t\t\t\t\t<div class=\"col-310 last\">
\t\t\t\t\t        </div>\t
\t\t\t\t\t</div>
\t\t\t\t\t
\t\t\t\t\t<h1 class=\"m-top-30\">Gallery</h1>
\t\t\t\t\t<ul class=\"gallery fixed\">
\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t<div><a href=\"";
        // line 133
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("publicAdmin/_content/gallery-img1.png"), "html", null, true);
        echo "\" rel=\"gallery')}}\"></a></div>
\t\t\t\t\t\t\t<img src=\"";
        // line 134
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("publicAdmin/_content/gallery-img1.png"), "html", null, true);
        echo "\" alt=\"\" />
\t\t\t\t\t\t</li>
\t\t\t\t\t\t
\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t<div><a href=\"";
        // line 138
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("publicAdmin/_content/gallery-img1-original.png"), "html", null, true);
        echo "\" rel=\"gallery\"></a></div>
\t\t\t\t\t\t\t<img src=\"";
        // line 139
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("publicAdmin/_content/gallery-img2.png"), "html", null, true);
        echo "\" alt=\"\" />
\t\t\t\t\t\t</li>
\t\t\t\t\t\t
\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t<div><a href=\"";
        // line 143
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("publicAdmin/_content/gallery-img3-original.png"), "html", null, true);
        echo "\" rel=\"gallery\"></a></div>
\t\t\t\t\t\t\t<img src=\"";
        // line 144
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("publicAdmin/_content/gallery-img3.png"), "html", null, true);
        echo "\" alt=\"\" />
\t\t\t\t\t\t</li>
\t\t\t\t\t\t
\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t<div><a href=\"";
        // line 148
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("publicAdmin/_content/gallery-img4-original.png"), "html", null, true);
        echo "\" rel=\"gallery\"></a></div>
\t\t\t\t\t\t\t<img src=\"";
        // line 149
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("publicAdmin/_content/gallery-img4.png"), "html", null, true);
        echo "\" alt=\"\" />
\t\t\t\t\t\t</li>
\t\t\t\t\t\t
\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t<div><a href=\"";
        // line 153
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("publicAdmin/_content/gallery-img5-original.png"), "html", null, true);
        echo "\" rel=\"gallery\"></a></div>
\t\t\t\t\t\t\t<img src=\"";
        // line 154
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("publicAdmin/_content/gallery-img5.png"), "html", null, true);
        echo "\" alt=\"\" />
\t\t\t\t\t\t</li>
\t\t\t\t\t\t
\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t<div><a href=\"";
        // line 158
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("publicAdmin/_content/gallery-img6-original.png"), "html", null, true);
        echo "\" rel=\"gallery\"></a></div>
\t\t\t\t\t\t\t<img src=\"";
        // line 159
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("publicAdmin/_content/gallery-img6.png"), "html", null, true);
        echo "\" alt=\"\" />
\t\t\t\t\t\t</li>
\t\t\t\t\t\t
\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t<div><a href=\"";
        // line 163
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("publicAdmin/_content/gallery-img7-original.png"), "html", null, true);
        echo "\" rel=\"gallery\"></a></div>
\t\t\t\t\t\t\t<img src=\"";
        // line 164
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("publicAdmin/_content/gallery-img7.png"), "html", null, true);
        echo "\" alt=\"\" />
\t\t\t\t\t\t</li>
\t\t\t\t\t\t
\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t<div><a href=\"";
        // line 168
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("publicAdmin/_content/gallery-img8-original.png"), "html", null, true);
        echo "\" rel=\"gallery\"></a></div>
\t\t\t\t\t\t\t<img src=\"";
        // line 169
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("publicAdmin/_content/gallery-img8.png"), "html", null, true);
        echo "\" alt=\"\" />
\t\t\t\t\t\t</li>
\t\t\t\t\t\t
\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t<div><a href=\"";
        // line 173
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("publicAdmin/_content/gallery-img9-original.png"), "html", null, true);
        echo "\" rel=\"gallery\"></a></div>
\t\t\t\t\t\t\t<img src=\"";
        // line 174
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("publicAdmin/_content/gallery-img9.png"), "html", null, true);
        echo "\" alt=\"\" />
\t\t\t\t\t\t</li>
\t\t\t\t\t\t
\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t<div><a href=\"";
        // line 178
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("publicAdmin/_content/gallery-img10-original.png"), "html", null, true);
        echo "\" rel=\"gallery\"></a></div>
\t\t\t\t\t\t\t<img src=\"";
        // line 179
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("publicAdmin/_content/gallery-img10.png"), "html", null, true);
        echo "\" alt=\"\" />
\t\t\t\t\t\t</li>
\t\t\t\t\t</ul>
\t\t\t\t\t
\t\t\t\t\t
\t\t\t\t
\t\t\t</div>
\t\t</div>

\t
\t
\t
\t 
\t</body>
\t

<!-- Mirrored from www.nicolaegabriel.info/live/admin-pure/ by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 20 Feb 2016 22:22:20 GMT -->
</html>
\t";
    }

    // line 120
    public function block_affichage($context, array $blocks = array())
    {
        // line 121
        echo "
                                                             ";
    }

    public function getTemplateName()
    {
        return "MoocBundle:user:dashboard.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  317 => 121,  314 => 120,  291 => 179,  287 => 178,  280 => 174,  276 => 173,  269 => 169,  265 => 168,  258 => 164,  254 => 163,  247 => 159,  243 => 158,  236 => 154,  232 => 153,  225 => 149,  221 => 148,  214 => 144,  210 => 143,  203 => 139,  199 => 138,  192 => 134,  188 => 133,  175 => 122,  173 => 120,  145 => 95,  129 => 82,  120 => 76,  113 => 72,  94 => 58,  61 => 28,  53 => 23,  46 => 19,  40 => 16,  36 => 15,  20 => 1,);
    }
}
/* */
/* */
/* <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"*/
/*     "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd"> */
/* <html dir="ltr" lang="en-US" xmlns="http://www.w3.org/1999/xhtml">*/
/* */
/* 	*/
/* <!-- Mirrored from www.nicolaegabriel.info/live/admin-pure/ by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 20 Feb 2016 22:21:15 GMT -->*/
/* <head>*/
/* 		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />*/
/* 		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />*/
/* */
/* 		<title>HTML - Admin Pure</title>*/
/* 		*/
/* 		<link rel='stylesheet' href="{{ asset('publicAdmin/_layout/scripts/jquery.fullcalendar/fullcalendar.css')}}" type='text/css' media='screen' />*/
/* 		<link rel='stylesheet' href="{{ asset('publicAdmin/_layout/scripts/jquery.fullcalendar/fullcalendar.print.css')}}" type='text/css' media='print' />*/
/* 		*/
/* 		<!-- Styles -->*/
/* 		<link rel='stylesheet' href="{{ asset('publicAdmin/_layout/style.css')}}" type='text/css' media='all' />*/
/* 		*/
/* 		<!--[if IE]>*/
/* 		*/
/* 			<link rel='stylesheet' href="{{ asset('publicAdmin/_layout/IE.css')}}" type='text/css' media='all'')}}" />		*/
/* 			*/
/* 		<![endif]-->*/
/* 		*/
/* 		<!-- Fonts -->*/
/* 		<link href="{{ asset('http://fonts.googleapis.com/css?family=Droid+Sans:regular,bold|PT+Sans+Narrow:regular,bold|Droid+Serif:i&amp;v1')}}" rel='stylesheet' type='text/css' />*/
/* 		*/
/* 		<script type='text/javascript' src='../../../ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min97e1.js?ver=1.7'></script>*/
/* 		<script type="" src="../../../ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/jquery-ui.min.js"></script>*/
/* 		*/
/* 		<!-- Calendar -->*/
/* 		<script type='text/javascript' src='publicAdmin/_layout/scripts/jquery.fullcalendar/fullcalendar.min.js'></script>*/
/* 		*/
/* 		<!-- Scripts -->*/
/* 		<script type='text/javascript' src='publicAdmin/_layout/custom.js'></script>*/
/* 		*/
/* 	</head>  */
/*   */
/* 	<body>*/
/* 	 */
/* 		<div id="layout">*/
/* 			<div id="header-wrapper">*/
/* 				<div id="header">*/
/* 					<div id="user-wrapper" class="fixed">*/
/* 						<div class="color-scheme">*/
/* 							<a href="#" class="button">Dropdown suggestion</a>*/
/* 						</div>*/
/* 						<div class="user">*/
/* 							<span>Welcome <a href="#">Ghada !</a></span>*/
/* 							<span class="logout"><a href="http://localhost/moocApp/web/app_dev.php/logout">Logout</a></span>*/
/* 						</div>*/
/* 					</div>*/
/* 					*/
/* 					<div id="launcher-wrapper" class="fixed">*/
/* 						<div class="logo">*/
/* 							<a href="{{ asset('index-2.html')}}"><img src="{{ asset('publicAdmin/_content/back-logo.png')}}" alt="" /></a>*/
/* 						</div>*/
/* 						*/
/* 						<div class="launcher">*/
/* 							*/
/* 						</div>*/
/* 					</div>*/
/* 				</div>*/
/* 			</div>*/
/* 			*/
/* 			<div class="page fixed">*/
/* 				<div id="sidebar">*/
/* 					<ul id="navigation">*/
/* 						<li class="first active">*/
/* 							<div><a href="{{ asset('index-2.html')}}">Dashboard</a><span class="icon-nav dashboard"></span></div>*/
/* 							<div class="back"></div>*/
/* 						</li>*/
/* 						<li>*/
/* 							<div><a href="{{ path('valider_organisme')}}">Valider organisme</a><span class="icon-nav tables"></span></div>*/
/* 							<div class="back"></div>*/
/* 						</li>*/
/* 						*/
/* 						*/
/* 						<li class="last">*/
/* 							<div><a href="{{ path('afficher_compte_admin')}}">Liste des comptes</a><span class="icon-nav users"></span></div>*/
/* 							<div class="back"></div>*/
/* 						</li>*/
/* 					</ul>*/
/* 				</div>*/
/* 				*/
/* 				*/
/* 				<div id="content">*/
/* 					<div class="fixed index-large-icon">*/
/* 						<a href="#" class="large-icon one"><span></span></a>*/
/* 						<a href="#" class="large-icon two"><span></span></a>*/
/* 						<a href="#" class="large-icon three"><span></span></a>*/
/* 						<a href="#" class="large-icon four"><span></span></a>*/
/* 						<a href="{{ path('afficher_compte_admin') }}" class="large-icon five"><span></span></a>*/
/* 						<a href="#" class="large-icon six"><span></span></a>*/
/* 						<a href="#" class="large-icon seven last"><span></span></a>*/
/* 						<a href="#" class="large-icon ten"><span></span></a>*/
/* 						<a href="#" class="large-icon eleven"><span></span></a>*/
/* 						<a href="#" class="large-icon twelve"><span></span></a>*/
/* 						<a href="#" class="large-icon thirteen"><span></span></a>*/
/* 						<a href="#" class="large-icon fourteen"><span></span></a>*/
/* 						<a href="#" class="large-icon fifteen"><span></span></a>*/
/* 						<a href="#" class="large-icon sixteen last"><span></span></a>*/
/* 					</div>*/
/* 					*/
/* 					<div class="notice-one">Hi there! I’m just a warning.*/
/* 						<span></span>*/
/* 					</div>*/
/* 					*/
/* 					*/
/* 					*/
/* 				*/
/* 					<div class="hr"></div>  */
/* 					*/
/* 					<div class="fixed">*/
/* 						<div class="col-310">*/
/* 							<div class="panel">*/
/*                                                             */
/*                                                              {% block affichage %}*/
/* */
/*                                                              {% endblock %}	*/
/* 							</div>*/
/* 						</div>*/
/* 						*/
/* 						<div class="col-310 last">*/
/* 					        </div>	*/
/* 					</div>*/
/* 					*/
/* 					<h1 class="m-top-30">Gallery</h1>*/
/* 					<ul class="gallery fixed">*/
/* 						<li>*/
/* 							<div><a href="{{ asset('publicAdmin/_content/gallery-img1.png')}}" rel="gallery')}}"></a></div>*/
/* 							<img src="{{ asset('publicAdmin/_content/gallery-img1.png')}}" alt="" />*/
/* 						</li>*/
/* 						*/
/* 						<li>*/
/* 							<div><a href="{{ asset('publicAdmin/_content/gallery-img1-original.png')}}" rel="gallery"></a></div>*/
/* 							<img src="{{ asset('publicAdmin/_content/gallery-img2.png')}}" alt="" />*/
/* 						</li>*/
/* 						*/
/* 						<li>*/
/* 							<div><a href="{{ asset('publicAdmin/_content/gallery-img3-original.png')}}" rel="gallery"></a></div>*/
/* 							<img src="{{ asset('publicAdmin/_content/gallery-img3.png')}}" alt="" />*/
/* 						</li>*/
/* 						*/
/* 						<li>*/
/* 							<div><a href="{{ asset('publicAdmin/_content/gallery-img4-original.png')}}" rel="gallery"></a></div>*/
/* 							<img src="{{ asset('publicAdmin/_content/gallery-img4.png')}}" alt="" />*/
/* 						</li>*/
/* 						*/
/* 						<li>*/
/* 							<div><a href="{{ asset('publicAdmin/_content/gallery-img5-original.png')}}" rel="gallery"></a></div>*/
/* 							<img src="{{ asset('publicAdmin/_content/gallery-img5.png')}}" alt="" />*/
/* 						</li>*/
/* 						*/
/* 						<li>*/
/* 							<div><a href="{{ asset('publicAdmin/_content/gallery-img6-original.png')}}" rel="gallery"></a></div>*/
/* 							<img src="{{ asset('publicAdmin/_content/gallery-img6.png')}}" alt="" />*/
/* 						</li>*/
/* 						*/
/* 						<li>*/
/* 							<div><a href="{{ asset('publicAdmin/_content/gallery-img7-original.png')}}" rel="gallery"></a></div>*/
/* 							<img src="{{ asset('publicAdmin/_content/gallery-img7.png')}}" alt="" />*/
/* 						</li>*/
/* 						*/
/* 						<li>*/
/* 							<div><a href="{{ asset('publicAdmin/_content/gallery-img8-original.png')}}" rel="gallery"></a></div>*/
/* 							<img src="{{ asset('publicAdmin/_content/gallery-img8.png')}}" alt="" />*/
/* 						</li>*/
/* 						*/
/* 						<li>*/
/* 							<div><a href="{{ asset('publicAdmin/_content/gallery-img9-original.png')}}" rel="gallery"></a></div>*/
/* 							<img src="{{ asset('publicAdmin/_content/gallery-img9.png')}}" alt="" />*/
/* 						</li>*/
/* 						*/
/* 						<li>*/
/* 							<div><a href="{{ asset('publicAdmin/_content/gallery-img10-original.png')}}" rel="gallery"></a></div>*/
/* 							<img src="{{ asset('publicAdmin/_content/gallery-img10.png')}}" alt="" />*/
/* 						</li>*/
/* 					</ul>*/
/* 					*/
/* 					*/
/* 				*/
/* 			</div>*/
/* 		</div>*/
/* */
/* 	*/
/* 	*/
/* 	*/
/* 	 */
/* 	</body>*/
/* 	*/
/* */
/* <!-- Mirrored from www.nicolaegabriel.info/live/admin-pure/ by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 20 Feb 2016 22:22:20 GMT -->*/
/* </html>*/
/* 	*/
