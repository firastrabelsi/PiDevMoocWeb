<?php

/* MoocBundle:mail:new.html.twig */
class __TwigTemplate_ac132dd903924f19ef094b57248616fc922ec5e5321fdaf705b8c3092c025cd3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "<body> 

 <h2> <strong> Formulaire De Contact</strong> </h2>

 <hr>

 <h3><p> Contacter nous </p></h3>

 <p>";
        // line 10
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
        echo "</p>

 <hr>

 <form role=\"form\" id=\"fr\" method=\"POST\" action='";
        // line 14
        echo $this->env->getExtension('routing')->getPath("my_app_mail_sendpage");
        echo "' > 

 ";
        // line 16
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        echo "

 </form>

</body>";
    }

    public function getTemplateName()
    {
        return "MoocBundle:mail:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  41 => 16,  36 => 14,  29 => 10,  19 => 2,);
    }
}
/* {# empty Twig template #}*/
/* <body> */
/* */
/*  <h2> <strong> Formulaire De Contact</strong> </h2>*/
/* */
/*  <hr>*/
/* */
/*  <h3><p> Contacter nous </p></h3>*/
/* */
/*  <p>{{form_errors(form)}}</p>*/
/* */
/*  <hr>*/
/* */
/*  <form role="form" id="fr" method="POST" action='{{path('my_app_mail_sendpage')}}' > */
/* */
/*  {{form_widget(form)}}*/
/* */
/*  </form>*/
/* */
/* </body>*/
