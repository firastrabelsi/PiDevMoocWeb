<?php

/* MoocBundle:Registration:Form2.html.twig */
class __TwigTemplate_80290356bfd45279b52107709e991a957360367fce7c568bdb188cfa01d5d3fa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "
<h1> ";
        // line 3
        echo twig_escape_filter($this->env, (isset($context["email"]) ? $context["email"] : $this->getContext($context, "email")), "html", null, true);
        echo " </H1>
<form action=\"";
        // line 4
        echo $this->env->getExtension('routing')->getPath("registerstep2");
        echo "\" methode=\"POST\">
    <table>
        <tr>
            <td> <label>Nom :</label></td>
            <td><input type=\"text\" name=\"nom\"/></td>
        </tr>
        <tr>
            <td> <label>Prenom :</label></td>
            <td><input type=\"text\" name=\"prenom\"/></td>
        </tr>
        <tr>
            <td> <label>Adresse :</label></td>
            <td><input type=\"text\" name=\"adresse\"/></td>
        </tr>
        


        <tr>
            <td>    <input type=\"hidden\" name=\"login\" value=\"";
        // line 22
        echo twig_escape_filter($this->env, (isset($context["email"]) ? $context["email"] : $this->getContext($context, "email")), "html", null, true);
        echo "\"/>
</td>
            <td><input type=\"submit\" value=\"Valider\"/></td>
        </tr>
    </table>
</form>
";
    }

    public function getTemplateName()
    {
        return "MoocBundle:Registration:Form2.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  47 => 22,  26 => 4,  22 => 3,  19 => 2,);
    }
}
/* {# empty Twig template #}*/
/* */
/* <h1> {{email}} </H1>*/
/* <form action="{{path('registerstep2')}}" methode="POST">*/
/*     <table>*/
/*         <tr>*/
/*             <td> <label>Nom :</label></td>*/
/*             <td><input type="text" name="nom"/></td>*/
/*         </tr>*/
/*         <tr>*/
/*             <td> <label>Prenom :</label></td>*/
/*             <td><input type="text" name="prenom"/></td>*/
/*         </tr>*/
/*         <tr>*/
/*             <td> <label>Adresse :</label></td>*/
/*             <td><input type="text" name="adresse"/></td>*/
/*         </tr>*/
/*         */
/* */
/* */
/*         <tr>*/
/*             <td>    <input type="hidden" name="login" value="{{email}}"/>*/
/* </td>*/
/*             <td><input type="submit" value="Valider"/></td>*/
/*         </tr>*/
/*     </table>*/
/* </form>*/
/* */
