<?php

/* MoocBundle:user:modifier.html.twig */
class __TwigTemplate_422d1fca817bd513ac1659592b968ae2ef518a50a8d3d9f480568c5f61d57fc2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("PiTdocBundle:user:inedexClient.html.twig", "MoocBundle:user:modifier.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "PiTdocBundle:user:inedexClient.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "<form method='POST'></p>

    
    <div class=\"container panel-group\">

            
                <div class=\"row featurebox panel panel-success\" >
                    
                    <div class=\"panel-title \">
                        <h1 class=\"text-center\"><strong>Modifier mon compte</strong></h1>
                    </div>

                    <div class=\" panel-body\">

                        <div class=\"row \">
                            <center><div class=\" col-md-5 \"><span class=\"h4\">Nom</span> : </div>
                                <div class=\" col-md-5\"><span class=\"h4\"><input type=\"text\" name=\"nom\"/></span> </div>
                            </center>


                        </div>
                        </br>
                        <div class=\"row\">
                            <center><div class=\" col-md-5\"><span class=\"h4\">Prenom</span> : </div>
                                <div class=\" col-md-5\"><span class=\"h4\"><input type=\"text\" name=\"prenom\"/></span> </div></center>
                            
                            
                        </div>
                        </br>
                        <div class=\"row\">
                            <center><div class=\" col-md-5\"><span class=\"h4\">Mot de passe</span> : </div>
                                <div class=\" col-md-5\"><span class=\"h4\"><input type=\"text\" name=\"pwd\"/></span> </div></center>
                            
                            
                        </div>
                        
                    </div>

                    <div>
                            <div class=\"panel-footer  text-center\">
                                
                                <strong><input type=\"submit\" value=\"Valider\" class=\"btn btn-success\"/></strong>
                                            
                    </div>
                </div>
                    </div> 
           
        
    </div>
</form>
";
    }

    public function getTemplateName()
    {
        return "MoocBundle:user:modifier.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 4,  28 => 3,  11 => 1,);
    }
}
/* {% extends "PiTdocBundle:user:inedexClient.html.twig" %}*/
/* */
/* {% block body %}*/
/* <form method='POST'></p>*/
/* */
/*     */
/*     <div class="container panel-group">*/
/* */
/*             */
/*                 <div class="row featurebox panel panel-success" >*/
/*                     */
/*                     <div class="panel-title ">*/
/*                         <h1 class="text-center"><strong>Modifier mon compte</strong></h1>*/
/*                     </div>*/
/* */
/*                     <div class=" panel-body">*/
/* */
/*                         <div class="row ">*/
/*                             <center><div class=" col-md-5 "><span class="h4">Nom</span> : </div>*/
/*                                 <div class=" col-md-5"><span class="h4"><input type="text" name="nom"/></span> </div>*/
/*                             </center>*/
/* */
/* */
/*                         </div>*/
/*                         </br>*/
/*                         <div class="row">*/
/*                             <center><div class=" col-md-5"><span class="h4">Prenom</span> : </div>*/
/*                                 <div class=" col-md-5"><span class="h4"><input type="text" name="prenom"/></span> </div></center>*/
/*                             */
/*                             */
/*                         </div>*/
/*                         </br>*/
/*                         <div class="row">*/
/*                             <center><div class=" col-md-5"><span class="h4">Mot de passe</span> : </div>*/
/*                                 <div class=" col-md-5"><span class="h4"><input type="text" name="pwd"/></span> </div></center>*/
/*                             */
/*                             */
/*                         </div>*/
/*                         */
/*                     </div>*/
/* */
/*                     <div>*/
/*                             <div class="panel-footer  text-center">*/
/*                                 */
/*                                 <strong><input type="submit" value="Valider" class="btn btn-success"/></strong>*/
/*                                             */
/*                     </div>*/
/*                 </div>*/
/*                     </div> */
/*            */
/*         */
/*     </div>*/
/* </form>*/
/* {% endblock %}*/
/* */
