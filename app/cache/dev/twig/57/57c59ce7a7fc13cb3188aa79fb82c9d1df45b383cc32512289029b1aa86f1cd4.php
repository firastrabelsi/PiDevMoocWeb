<?php

/* MoocBundle:user:tables.html.twig  */
class __TwigTemplate_b866f089c8f8dca8b0a62bb3bbbb7f1af99bc619aeb66b40e1dacd70b1578f2f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'affichage' => array($this, 'block_affichage'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "

<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.1//EN\"
    \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\"> 
<html dir=\"ltr\" lang=\"en-US\" xmlns=\"http://www.w3.org/1999/xhtml\">

\t
<!-- Mirrored from www.nicolaegabriel.info/live/admin-pure/ by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 20 Feb 2016 22:21:15 GMT -->
<head>
\t\t<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />
\t\t<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge,chrome=1\" />

\t\t<title>HTML - Admin Pure</title>
\t\t
\t\t<link rel='stylesheet' href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("publicAdmin/_layout/scripts/jquery.fullcalendar/fullcalendar.css"), "html", null, true);
        echo "\" type='text/css' media='screen' />
\t\t<link rel='stylesheet' href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("publicAdmin/_layout/scripts/jquery.fullcalendar/fullcalendar.print.css"), "html", null, true);
        echo "\" type='text/css' media='print' />
\t\t
\t\t<!-- Styles -->
\t\t<link rel='stylesheet' href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("publicAdmin/_layout/style.css"), "html", null, true);
        echo "\" type='text/css' media='all' />
\t\t
\t\t<!--[if IE]>
\t\t
\t\t\t<link rel='stylesheet' href=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("publicAdmin/_layout/IE.css"), "html", null, true);
        echo "\" type='text/css' media='all'')}}\" />\t\t
\t\t\t
\t\t<![endif]-->
\t\t
\t\t<!-- Fonts -->
\t\t<link href=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("http://fonts.googleapis.com/css?family=Droid+Sans:regular,bold|PT+Sans+Narrow:regular,bold|Droid+Serif:i&amp;v1"), "html", null, true);
        echo "\" rel='stylesheet' type='text/css' />
\t\t
\t\t<script type='text/javascript' src='../../../ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min97e1.js?ver=1.7'></script>
\t\t<script type=\"\" src=\"../../../ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/jquery-ui.min.js\"></script>
\t\t
\t\t<!-- Calendar -->
\t\t<script type='text/javascript' src='publicAdmin/_layout/scripts/jquery.fullcalendar/fullcalendar.min.js'></script>
\t\t
\t\t<!-- Scripts -->
\t\t<script type='text/javascript' src='publicAdmin/_layout/custom.js'></script>
\t\t
\t</head>  
  
\t<body>
\t 
\t\t<div id=\"layout\">
\t\t\t<div id=\"header-wrapper\">
\t\t\t\t<div id=\"header\">
\t\t\t\t\t<div id=\"user-wrapper\" class=\"fixed\">
\t\t\t\t\t\t<div class=\"color-scheme\">
\t\t\t\t\t\t\t<a href=\"#\" class=\"button\">Dropdown suggestion</a>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"user\">
\t\t\t\t\t\t\t<img src=\"";
        // line 51
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("publicAdmin/_content/user-img.png"), "html", null, true);
        echo "\" alt=\"\" />
\t\t\t\t\t\t\t<span>Welcome <a href=\"#\">ghada !</a></span>
\t\t\t\t\t\t\t<span class=\"";
        // line 53
        echo $this->env->getExtension('routing')->getPath("fos_user_security_logout");
        echo "}\"><a href=\"#\">Logout</a></span>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t
\t\t\t\t\t<div id=\"launcher-wrapper\" class=\"fixed\">
\t\t\t\t\t\t<div class=\"logo\">
\t\t\t\t\t\t\t<a href=\"";
        // line 59
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("index-2.html"), "html", null, true);
        echo "\"><img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("publicAdmin/_content/back-logo.png"), "html", null, true);
        echo "\" alt=\"\" /></a>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t
\t\t\t\t\t\t<div class=\"launcher\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t
\t\t\t<div class=\"page fixed\">
\t\t\t\t<div id=\"sidebar\">
\t\t\t\t\t<ul id=\"navigation\">
\t\t\t\t\t\t<li class=\"first active\">
\t\t\t\t\t\t\t<div><a href=\"";
        // line 73
        echo $this->env->getExtension('routing')->getPath("dashboard");
        echo "\">Dashboard</a><span class=\"icon-nav dashboard\"></span></div>
\t\t\t\t\t\t\t<div class=\"back\"></div>
\t\t\t\t\t\t</li>
\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t<div><a href=\"";
        // line 77
        echo $this->env->getExtension('routing')->getPath("valider_organisme");
        echo "\">Valider organisme</a><span class=\"icon-nav tables\"></span></div>
\t\t\t\t\t\t\t<div class=\"back\"></div>
\t\t\t\t\t\t</li>
\t\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t\t\t\t<li class=\"last\">
\t\t\t\t\t\t\t<div><a href=\"";
        // line 83
        echo $this->env->getExtension('routing')->getPath("afficher_compte_admin");
        echo "\">Liste des comptes</a><span class=\"icon-nav users\"></span></div>
\t\t\t\t\t\t\t<div class=\"back\"></div>
\t\t\t\t\t\t</li>
\t\t\t\t\t</ul>
\t\t\t\t</div>
\t\t\t\t
\t\t\t\t
\t\t\t\t<div id=\"content\">
\t\t\t\t
\t\t\t\t\t<ul class=\"breadcrumb\">
\t\t\t\t\t\t<li class=\"home\"><a href=\"";
        // line 93
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("index-2.html"), "html", null, true);
        echo "\" ></a></li>
\t\t\t\t\t\t<li><a href=\"#\" >Admin Pure</a></li>
\t\t\t\t\t\t<li><a href=\"#\" >Layouts</a></li>
\t\t\t\t\t\t<li class=\"last\"><a href=\"#\" >Tables</a></li>
\t\t\t\t\t</ul>
\t\t\t\t
\t\t\t\t\t
                                                
                                                
                                           ";
        // line 102
        $this->displayBlock('affichage', $context, $blocks);
        // line 104
        echo "     
                                                
                                                
                                                
\t\t\t\t\t
\t\t\t\t\t
\t\t\t\t\t
\t\t\t\t</div>
\t\t\t\t
\t\t\t</div>
\t\t</div>

\t
\t
\t
\t 
\t</body>
\t

<!-- Mirrored from www.nicolaegabriel.info/live/admin-pure/tables.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 20 Feb 2016 22:22:36 GMT -->
</html>
\t";
    }

    // line 102
    public function block_affichage($context, array $blocks = array())
    {
        // line 103
        echo "
                                                             ";
    }

    public function getTemplateName()
    {
        return "MoocBundle:user:tables.html.twig ";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  191 => 103,  188 => 102,  163 => 104,  161 => 102,  149 => 93,  136 => 83,  127 => 77,  120 => 73,  101 => 59,  92 => 53,  87 => 51,  61 => 28,  53 => 23,  46 => 19,  40 => 16,  36 => 15,  20 => 1,);
    }
}
/* */
/* */
/* <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"*/
/*     "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd"> */
/* <html dir="ltr" lang="en-US" xmlns="http://www.w3.org/1999/xhtml">*/
/* */
/* 	*/
/* <!-- Mirrored from www.nicolaegabriel.info/live/admin-pure/ by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 20 Feb 2016 22:21:15 GMT -->*/
/* <head>*/
/* 		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />*/
/* 		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />*/
/* */
/* 		<title>HTML - Admin Pure</title>*/
/* 		*/
/* 		<link rel='stylesheet' href="{{ asset('publicAdmin/_layout/scripts/jquery.fullcalendar/fullcalendar.css')}}" type='text/css' media='screen' />*/
/* 		<link rel='stylesheet' href="{{ asset('publicAdmin/_layout/scripts/jquery.fullcalendar/fullcalendar.print.css')}}" type='text/css' media='print' />*/
/* 		*/
/* 		<!-- Styles -->*/
/* 		<link rel='stylesheet' href="{{ asset('publicAdmin/_layout/style.css')}}" type='text/css' media='all' />*/
/* 		*/
/* 		<!--[if IE]>*/
/* 		*/
/* 			<link rel='stylesheet' href="{{ asset('publicAdmin/_layout/IE.css')}}" type='text/css' media='all'')}}" />		*/
/* 			*/
/* 		<![endif]-->*/
/* 		*/
/* 		<!-- Fonts -->*/
/* 		<link href="{{ asset('http://fonts.googleapis.com/css?family=Droid+Sans:regular,bold|PT+Sans+Narrow:regular,bold|Droid+Serif:i&amp;v1')}}" rel='stylesheet' type='text/css' />*/
/* 		*/
/* 		<script type='text/javascript' src='../../../ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min97e1.js?ver=1.7'></script>*/
/* 		<script type="" src="../../../ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/jquery-ui.min.js"></script>*/
/* 		*/
/* 		<!-- Calendar -->*/
/* 		<script type='text/javascript' src='publicAdmin/_layout/scripts/jquery.fullcalendar/fullcalendar.min.js'></script>*/
/* 		*/
/* 		<!-- Scripts -->*/
/* 		<script type='text/javascript' src='publicAdmin/_layout/custom.js'></script>*/
/* 		*/
/* 	</head>  */
/*   */
/* 	<body>*/
/* 	 */
/* 		<div id="layout">*/
/* 			<div id="header-wrapper">*/
/* 				<div id="header">*/
/* 					<div id="user-wrapper" class="fixed">*/
/* 						<div class="color-scheme">*/
/* 							<a href="#" class="button">Dropdown suggestion</a>*/
/* 						</div>*/
/* 						<div class="user">*/
/* 							<img src="{{ asset('publicAdmin/_content/user-img.png')}}" alt="" />*/
/* 							<span>Welcome <a href="#">ghada !</a></span>*/
/* 							<span class="{{path('fos_user_security_logout')}}}"><a href="#">Logout</a></span>*/
/* 						</div>*/
/* 					</div>*/
/* 					*/
/* 					<div id="launcher-wrapper" class="fixed">*/
/* 						<div class="logo">*/
/* 							<a href="{{ asset('index-2.html')}}"><img src="{{ asset('publicAdmin/_content/back-logo.png')}}" alt="" /></a>*/
/* 						</div>*/
/* 						*/
/* 						<div class="launcher">*/
/* 							*/
/* 						</div>*/
/* 					</div>*/
/* 				</div>*/
/* 			</div>*/
/* 			*/
/* 			<div class="page fixed">*/
/* 				<div id="sidebar">*/
/* 					<ul id="navigation">*/
/* 						<li class="first active">*/
/* 							<div><a href="{{ path('dashboard')}}">Dashboard</a><span class="icon-nav dashboard"></span></div>*/
/* 							<div class="back"></div>*/
/* 						</li>*/
/* 						<li>*/
/* 							<div><a href="{{ path('valider_organisme')}}">Valider organisme</a><span class="icon-nav tables"></span></div>*/
/* 							<div class="back"></div>*/
/* 						</li>*/
/* 						*/
/* 						*/
/* 						<li class="last">*/
/* 							<div><a href="{{ path('afficher_compte_admin')}}">Liste des comptes</a><span class="icon-nav users"></span></div>*/
/* 							<div class="back"></div>*/
/* 						</li>*/
/* 					</ul>*/
/* 				</div>*/
/* 				*/
/* 				*/
/* 				<div id="content">*/
/* 				*/
/* 					<ul class="breadcrumb">*/
/* 						<li class="home"><a href="{{ asset('index-2.html')}}" ></a></li>*/
/* 						<li><a href="#" >Admin Pure</a></li>*/
/* 						<li><a href="#" >Layouts</a></li>*/
/* 						<li class="last"><a href="#" >Tables</a></li>*/
/* 					</ul>*/
/* 				*/
/* 					*/
/*                                                 */
/*                                                 */
/*                                            {% block affichage %}*/
/* */
/*                                                              {% endblock %}     */
/*                                                 */
/*                                                 */
/*                                                 */
/* 					*/
/* 					*/
/* 					*/
/* 				</div>*/
/* 				*/
/* 			</div>*/
/* 		</div>*/
/* */
/* 	*/
/* 	*/
/* 	*/
/* 	 */
/* 	</body>*/
/* 	*/
/* */
/* <!-- Mirrored from www.nicolaegabriel.info/live/admin-pure/tables.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 20 Feb 2016 22:22:36 GMT -->*/
/* </html>*/
/* 	*/
