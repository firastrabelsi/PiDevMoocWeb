<?php

/* MoocBundle:admin:afficherDetailCompte.html.twig */
class __TwigTemplate_1e48081db30bff1d6f828944aa7f408610a3e3a43e64c88524a91c0cfae2c2dc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("MoocBundle:user:dashboard.html.twig ", "MoocBundle:admin:afficherDetailCompte.html.twig", 1);
        $this->blocks = array(
            'affichage' => array($this, 'block_affichage'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "MoocBundle:user:dashboard.html.twig ";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_affichage($context, array $blocks = array())
    {
        // line 4
        echo "    <div class=\"main_container\" id=\"dashboard_page\">
        <div class=\"row-fluid\">
            <div class=\"widget span8\" style=\"width: 100%\">
                <div class=\"widget-header \">
                    <i class=\"icon-user\"></i>
                    <h5>Detail compte</h5>
                </div>

                <div class=\"widget-body clearfix container-fluid\">
                    <div  class=\"row-fluid\">
                        
                        
                        
                        
                    </div>
                    <div class=\"row-fluid\" style=\"position: relative;left: 20%\">
                        <span>
                            <h3>
                                Nom: ";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["compte"]) ? $context["compte"] : $this->getContext($context, "compte")), "nom", array()), "html", null, true);
        echo "
                                <span style=\"position: relative;left: 10%\">Prenom: ";
        // line 23
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["compte"]) ? $context["compte"] : $this->getContext($context, "compte")), "prenom", array()), "html", null, true);
        echo "</span>
                            </h3>
                        </span>
                    </div>
                    <div class=\"row-fluid\" style=\"position: relative;left: 20%\">
                        <span>
                            <h3>
                                ";
        // line 30
        if ((($this->getAttribute($this->getAttribute((isset($context["compte"]) ? $context["compte"] : $this->getContext($context, "compte")), "roles", array()), 1, array(), "array") == "ROLE_TRADUCTEUR") || ($this->getAttribute($this->getAttribute((isset($context["compte"]) ? $context["compte"] : $this->getContext($context, "compte")), "roles", array()), 0, array(), "array") == "ROLE_TRADUCTEUR"))) {
            // line 31
            echo "                                    Role:Traducteur
                                ";
        } else {
            // line 33
            echo "                                    Role:Client
                                ";
        }
        // line 35
        echo "
                             
                                
                                
                            </h3>
                        </span>
                    </div>
                    ";
        // line 42
        if ((($this->getAttribute($this->getAttribute((isset($context["compte"]) ? $context["compte"] : $this->getContext($context, "compte")), "roles", array()), 1, array(), "array") == "ROLE_TRADUCTEUR") || ($this->getAttribute($this->getAttribute((isset($context["compte"]) ? $context["compte"] : $this->getContext($context, "compte")), "roles", array()), 0, array(), "array") == "ROLE_TRADUCTEUR"))) {
            // line 43
            echo "                        <div class=\"row-fluid\" style=\"position: relative;left: 20%\">
                            <span>
                                <h3>
                                    Langue: ";
            // line 46
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["compte"]) ? $context["compte"] : $this->getContext($context, "compte")), "langue", array()), "html", null, true);
            echo "
                                    <span style=\"position: relative;left: 4%\">Specialite:";
            // line 47
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["compte"]) ? $context["compte"] : $this->getContext($context, "compte")), "specialite", array()), "html", null, true);
            echo "</span>
                                </h3>
                            </span>
                        </div>
                    ";
        }
        // line 52
        echo "                    <div class=\"row-fluid\" style=\"position: relative;left: 20%;top:10%\">
                        <span>
                            ";
        // line 54
        if (($this->getAttribute((isset($context["compte"]) ? $context["compte"] : $this->getContext($context, "compte")), "etat", array()) == "valide")) {
            // line 55
            echo "
                                <a ><i class=\"icon-envelope\" style=\"margin-right: 10px;\"></i></a>
                                <a href=\"";
            // line 57
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("choix_admin_compte", array("choix" => 2, "id" => $this->getAttribute((isset($context["compte"]) ? $context["compte"] : $this->getContext($context, "compte")), "id", array()))), "html", null, true);
            echo "\"><i class=\"icon-remove\"style=\"margin-right: 10px;\" ></i></a>
                                <a href=\"\"><i class=\"icon-list\"style=\"margin-right: 10px;\" ></i></a>

                            ";
        } elseif (($this->getAttribute(        // line 60
(isset($context["compte"]) ? $context["compte"] : $this->getContext($context, "compte")), "etat", array()) == "masquer")) {
            // line 61
            echo "
                                <a href=\"";
            // line 62
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("choix_admin_compte", array("choix" => 3, "id" => $this->getAttribute((isset($context["compte"]) ? $context["compte"] : $this->getContext($context, "compte")), "id", array()))), "html", null, true);
            echo "\"><i class=\"icon-refresh\"style=\"margin-right: 10px;\" ></i></a>

                            ";
        } else {
            // line 65
            echo "
                                <a href=\"";
            // line 66
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("choix_admin_compte", array("choix" => 2, "id" => $this->getAttribute((isset($context["compte"]) ? $context["compte"] : $this->getContext($context, "compte")), "id", array()))), "html", null, true);
            echo "\"><i class=\"icon-remove\"style=\"margin-right: 10px;\" ></i></a>
                                <a href=\"";
            // line 67
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("choix_admin_compte", array("choix" => 1, "id" => $this->getAttribute((isset($context["compte"]) ? $context["compte"] : $this->getContext($context, "compte")), "id", array()))), "html", null, true);
            echo "\"><i class=\"icon-thumbs-up\"style=\"margin-right: 10px;\" ></i></a>

                            ";
        }
        // line 70
        echo "                        </span>
                    </div>
                </div>
            </div>

        </div>
          ";
        // line 76
        if ((($this->getAttribute($this->getAttribute((isset($context["compte"]) ? $context["compte"] : $this->getContext($context, "compte")), "roles", array()), 1, array(), "array") == "ROLE_TRADUCTEUR") || ($this->getAttribute($this->getAttribute((isset($context["compte"]) ? $context["compte"] : $this->getContext($context, "compte")), "roles", array()), 0, array(), "array") == "ROLE_TRADUCTEUR"))) {
            echo "             
        <div class=\"rightboxes\"style=\"position: relative;left:85%;top:50%;\"><a href='";
            // line 77
            echo $this->env->getExtension('routing')->getPath("afficher_compte_admin");
            echo "'  class=\"btn btn-primary\">Liste des comptes</a></div>

        <div class=\"row-fluid\">

            <h2 class=\"heading\">
                Analytics

            </h2>
        </div> 

        <div class=\"row-fluid\">
            <div class=\"widget-top widget widget-padding\">
                <div class=\"widget-header\"><i class=\"icon-signal\"></i>
                    <div class=\"widget-buttons\">
                        <a href=\"#\" data-title=\"Collapse\" data-collapsed=\"false\" class=\"tip collapse\"></a>
                    </div>
                </div>
                <div class=\"widget-body\">
                    <script src=\"http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js\" type=\"text/javascript\"></script>
                    <script src=\"//code.highcharts.com/4.0.1/highcharts.js\"></script>
                    <script src=\"//code.highcharts.com/4.0.1/modules/exporting.js\"></script>
                    <script type=\"text/javascript\">
                       
                    </script>
                    <div id=\"piechart\" style=\"min-width: 400px; height: 400px; margin: 0 auto\"></div>
                </div>
            </div>
        </div>
                    ";
        }
        // line 106
        echo "    </div>
                    

</div>
";
    }

    public function getTemplateName()
    {
        return "MoocBundle:admin:afficherDetailCompte.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  187 => 106,  155 => 77,  151 => 76,  143 => 70,  137 => 67,  133 => 66,  130 => 65,  124 => 62,  121 => 61,  119 => 60,  113 => 57,  109 => 55,  107 => 54,  103 => 52,  95 => 47,  91 => 46,  86 => 43,  84 => 42,  75 => 35,  71 => 33,  67 => 31,  65 => 30,  55 => 23,  51 => 22,  31 => 4,  28 => 3,  11 => 1,);
    }
}
/* {% extends "MoocBundle:user:dashboard.html.twig " %}*/
/* */
/* {% block affichage %}*/
/*     <div class="main_container" id="dashboard_page">*/
/*         <div class="row-fluid">*/
/*             <div class="widget span8" style="width: 100%">*/
/*                 <div class="widget-header ">*/
/*                     <i class="icon-user"></i>*/
/*                     <h5>Detail compte</h5>*/
/*                 </div>*/
/* */
/*                 <div class="widget-body clearfix container-fluid">*/
/*                     <div  class="row-fluid">*/
/*                         */
/*                         */
/*                         */
/*                         */
/*                     </div>*/
/*                     <div class="row-fluid" style="position: relative;left: 20%">*/
/*                         <span>*/
/*                             <h3>*/
/*                                 Nom: {{compte.nom}}*/
/*                                 <span style="position: relative;left: 10%">Prenom: {{compte.prenom}}</span>*/
/*                             </h3>*/
/*                         </span>*/
/*                     </div>*/
/*                     <div class="row-fluid" style="position: relative;left: 20%">*/
/*                         <span>*/
/*                             <h3>*/
/*                                 {% if compte.roles[1]=='ROLE_TRADUCTEUR' or compte.roles[0]=='ROLE_TRADUCTEUR'  %}*/
/*                                     Role:Traducteur*/
/*                                 {% else %}*/
/*                                     Role:Client*/
/*                                 {%endif%}*/
/* */
/*                              */
/*                                 */
/*                                 */
/*                             </h3>*/
/*                         </span>*/
/*                     </div>*/
/*                     {% if compte.roles[1]=='ROLE_TRADUCTEUR' or compte.roles[0]=='ROLE_TRADUCTEUR'%}*/
/*                         <div class="row-fluid" style="position: relative;left: 20%">*/
/*                             <span>*/
/*                                 <h3>*/
/*                                     Langue: {{compte.langue}}*/
/*                                     <span style="position: relative;left: 4%">Specialite:{{compte.specialite}}</span>*/
/*                                 </h3>*/
/*                             </span>*/
/*                         </div>*/
/*                     {%endif%}*/
/*                     <div class="row-fluid" style="position: relative;left: 20%;top:10%">*/
/*                         <span>*/
/*                             {% if compte.etat=='valide'%}*/
/* */
/*                                 <a ><i class="icon-envelope" style="margin-right: 10px;"></i></a>*/
/*                                 <a href="{{path('choix_admin_compte',{'choix':2,'id':compte.id})}}"><i class="icon-remove"style="margin-right: 10px;" ></i></a>*/
/*                                 <a href=""><i class="icon-list"style="margin-right: 10px;" ></i></a>*/
/* */
/*                             {%elseif compte.etat=='masquer'%}*/
/* */
/*                                 <a href="{{path('choix_admin_compte',{'choix':3,'id':compte.id})}}"><i class="icon-refresh"style="margin-right: 10px;" ></i></a>*/
/* */
/*                             {%else %}*/
/* */
/*                                 <a href="{{path('choix_admin_compte',{'choix':2,'id':compte.id})}}"><i class="icon-remove"style="margin-right: 10px;" ></i></a>*/
/*                                 <a href="{{path('choix_admin_compte',{'choix':1,'id':compte.id})}}"><i class="icon-thumbs-up"style="margin-right: 10px;" ></i></a>*/
/* */
/*                             {%endif%}*/
/*                         </span>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/* */
/*         </div>*/
/*           {% if compte.roles[1]=='ROLE_TRADUCTEUR' or compte.roles[0]=='ROLE_TRADUCTEUR'  %}             */
/*         <div class="rightboxes"style="position: relative;left:85%;top:50%;"><a href='{{path('afficher_compte_admin')}}'  class="btn btn-primary">Liste des comptes</a></div>*/
/* */
/*         <div class="row-fluid">*/
/* */
/*             <h2 class="heading">*/
/*                 Analytics*/
/* */
/*             </h2>*/
/*         </div> */
/* */
/*         <div class="row-fluid">*/
/*             <div class="widget-top widget widget-padding">*/
/*                 <div class="widget-header"><i class="icon-signal"></i>*/
/*                     <div class="widget-buttons">*/
/*                         <a href="#" data-title="Collapse" data-collapsed="false" class="tip collapse"></a>*/
/*                     </div>*/
/*                 </div>*/
/*                 <div class="widget-body">*/
/*                     <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>*/
/*                     <script src="//code.highcharts.com/4.0.1/highcharts.js"></script>*/
/*                     <script src="//code.highcharts.com/4.0.1/modules/exporting.js"></script>*/
/*                     <script type="text/javascript">*/
/*                        */
/*                     </script>*/
/*                     <div id="piechart" style="min-width: 400px; height: 400px; margin: 0 auto"></div>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*                     {%endif%}*/
/*     </div>*/
/*                     */
/* */
/* </div>*/
/* {%endblock %} */
/* */
