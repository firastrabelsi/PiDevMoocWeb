<?php

/* MoocBundle:admin:afficherComptesOrganismes.html.twig */
class __TwigTemplate_84b201c0ab3fde0e945fc6a287ea9da695cee541d86fd2638725a20f34ee2abc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("MoocBundle:user:tables.html.twig ", "MoocBundle:admin:afficherComptesOrganismes.html.twig", 1);
        $this->blocks = array(
            'affichage' => array($this, 'block_affichage'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "MoocBundle:user:tables.html.twig ";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_affichage($context, array $blocks = array())
    {
        // line 3
        echo "    <div class=\"main_container\">

        <div class=\"row-fluid\">
            <div class=\"widget widget-padding span12\">
                <div class=\"widget-header\">
                    <i class=\"icon-group\"></i>
                    <h5>Utilisateur</h5>

                </div>  
                <div class=\"widget-body\">
                    <table  class=\"table table-striped table-bordered dataTable\">
                        <thead>
                            <tr>
                                <th>Nom d'utlisateur</th>
                                <th>Type </th>
                                <th>Etat </th>
                                <th>Action </th>


                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            ";
        // line 26
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["comptes"]) ? $context["comptes"] : $this->getContext($context, "comptes")));
        foreach ($context['_seq'] as $context["_key"] => $context["compte"]) {
            // line 27
            echo "                                ";
            if (($this->getAttribute($this->getAttribute($context["compte"], "roles", array()), 0, array(), "array") == "ROLE_ORGANISME")) {
                // line 28
                echo "                                    <tr>
                                        <td>";
                // line 29
                echo twig_escape_filter($this->env, $this->getAttribute($context["compte"], "username", array()), "html", null, true);
                echo "</td>
                                        <td>ORGANISME</td>

                                        ";
                // line 32
                if (($this->getAttribute($context["compte"], "etat", array()) == "valide")) {
                    // line 33
                    echo "                                            <td><span class=\"label label-success\">Valide</span></td> 
                                        ";
                } elseif (($this->getAttribute(                // line 34
$context["compte"], "etat", array()) == "non valide")) {
                    // line 35
                    echo "                                            <td><span class=\"label label-important\"> Non Valide</span></td>
                                        ";
                } else {
                    // line 37
                    echo "                                            <td><span class=\"label\">Valide a terminer</span></td>   
                                        ";
                }
                // line 39
                echo "                                        <td><a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("supp_compte1", array("id" => $this->getAttribute($context["compte"], "id", array()))), "html", null, true);
                echo "}\">Supprimer</a></td>
                                        <td><a href=\"";
                // line 40
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("valider_organismeaction", array("id" => $this->getAttribute($context["compte"], "id", array()))), "html", null, true);
                echo "}\">Valider</a></td>


                                    </tr>
                                ";
            }
            // line 45
            echo "                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['compte'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 46
        echo "                        </tbody>
                    </table>
                </div> <!-- /widget-body -->
            </div> <!-- /widget -->
        </div> <!-- /row-fluid -->
        <div class=\"row-fluid\">
            <form action=\"";
        // line 52
        echo $this->env->getExtension('routing')->getPath("cher");
        echo "\" methode=\"Post\">
                <table>
                    <tr>
                        <td> <label>Login</label></td>
                        <td><input type=\"text\" name=\"login\"/></td>
                    </tr>


                    <tr>
                        <td></td>
                        <td><input type=\"submit\" value=\"chercher\"/></td>
                    </tr>
                </table>
            </form>

        </div>

    </div>




";
    }

    public function getTemplateName()
    {
        return "MoocBundle:admin:afficherComptesOrganismes.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  114 => 52,  106 => 46,  100 => 45,  92 => 40,  87 => 39,  83 => 37,  79 => 35,  77 => 34,  74 => 33,  72 => 32,  66 => 29,  63 => 28,  60 => 27,  56 => 26,  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {% extends "MoocBundle:user:tables.html.twig " %}*/
/* {% block affichage %}*/
/*     <div class="main_container">*/
/* */
/*         <div class="row-fluid">*/
/*             <div class="widget widget-padding span12">*/
/*                 <div class="widget-header">*/
/*                     <i class="icon-group"></i>*/
/*                     <h5>Utilisateur</h5>*/
/* */
/*                 </div>  */
/*                 <div class="widget-body">*/
/*                     <table  class="table table-striped table-bordered dataTable">*/
/*                         <thead>*/
/*                             <tr>*/
/*                                 <th>Nom d'utlisateur</th>*/
/*                                 <th>Type </th>*/
/*                                 <th>Etat </th>*/
/*                                 <th>Action </th>*/
/* */
/* */
/*                                 <th></th>*/
/*                             </tr>*/
/*                         </thead>*/
/*                         <tbody>*/
/*                             {% for compte in comptes%}*/
/*                                 {%if compte.roles[0]=='ROLE_ORGANISME'%}*/
/*                                     <tr>*/
/*                                         <td>{{compte.username}}</td>*/
/*                                         <td>ORGANISME</td>*/
/* */
/*                                         {% if compte.etat=='valide'%}*/
/*                                             <td><span class="label label-success">Valide</span></td> */
/*                                         {%elseif compte.etat=='non valide'%}*/
/*                                             <td><span class="label label-important"> Non Valide</span></td>*/
/*                                         {%else %}*/
/*                                             <td><span class="label">Valide a terminer</span></td>   */
/*                                         {%endif%}*/
/*                                         <td><a href="{{path('supp_compte1',{'id':compte.id})}}}">Supprimer</a></td>*/
/*                                         <td><a href="{{path('valider_organismeaction',{'id':compte.id})}}}">Valider</a></td>*/
/* */
/* */
/*                                     </tr>*/
/*                                 {%endif%}*/
/*                             {% endfor %}*/
/*                         </tbody>*/
/*                     </table>*/
/*                 </div> <!-- /widget-body -->*/
/*             </div> <!-- /widget -->*/
/*         </div> <!-- /row-fluid -->*/
/*         <div class="row-fluid">*/
/*             <form action="{{path('cher')}}" methode="Post">*/
/*                 <table>*/
/*                     <tr>*/
/*                         <td> <label>Login</label></td>*/
/*                         <td><input type="text" name="login"/></td>*/
/*                     </tr>*/
/* */
/* */
/*                     <tr>*/
/*                         <td></td>*/
/*                         <td><input type="submit" value="chercher"/></td>*/
/*                     </tr>*/
/*                 </table>*/
/*             </form>*/
/* */
/*         </div>*/
/* */
/*     </div>*/
/* */
/* */
/* */
/* */
/* {%endblock%}*/
/* */
