<?php

/* MoocQuizBundle:PasserQuiz:QuizFinal.html.twig */
class __TwigTemplate_2fcf9635b3a6bc52f904e24b2ed6eec3e9e82a9a251a4759348ff0ec69364641 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("MoocQuizBundle::layout.html.twig", "MoocQuizBundle:PasserQuiz:QuizFinal.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "MoocQuizBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 5
        echo "<script>
        function countDown(secs, elem) {
        var element = document.getElementById(elem);
                element.innerHTML = \"Il vous reste : \" + Math.floor(secs / 60) + \" minutes et  \" + secs % 60 + \" seconde \";
                if (secs <= 1) {
        clearTimeout(timer);
                window.document.form.text4.value = \"timer\";
                element.innerHTML = checkVal(this.form);
                window.document.form.submit();
        }
        secs--;
                var timer = setTimeout('countDown(' + secs + ',\"' + elem + '\")', 1000);
        }
    </script>




<script  language=\"javascript\">
function checkVal(form) {
var point = 0;
var score = 0;";
        // line 26
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["questions"]) ? $context["questions"] : $this->getContext($context, "questions")));
        foreach ($context['_seq'] as $context["_key"] => $context["question"]) {
            // line 27
            echo "point = point +";
            echo twig_escape_filter($this->env, $this->getAttribute($context["question"], "point", array()), "html", null, true);
            echo " ; 
           ";
            // line 28
            if (($this->getAttribute($context["question"], "type", array()) == "Choix multiple")) {
                // line 29
                echo "
var resultat = false;
var x = document.form.radio";
                // line 31
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["reponse"]) ? $context["reponse"] : $this->getContext($context, "reponse")), "id", array()), "html", null, true);
                echo ".value;                    
";
                // line 32
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["reponse"]) ? $context["reponse"] : $this->getContext($context, "reponse")));
                foreach ($context['_seq'] as $context["_key"] => $context["rep"]) {
                    // line 33
                    if (($this->getAttribute($context["question"], "id", array()) == $this->getAttribute($this->getAttribute($context["rep"], "idquestion", array()), "id", array()))) {
                        // line 34
                        if (($this->getAttribute($context["rep"], "reponsecorrectradio", array()) == true)) {
                            // line 35
                            echo "

if (x == \"";
                            // line 37
                            echo twig_escape_filter($this->env, $this->getAttribute($context["rep"], "reponsecorrecttext", array()), "html", null, true);
                            echo "\")
{
resultat = true;
}

";
                        }
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['rep'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 45
                echo "if (resultat == true)
{
score = score +";
                // line 47
                echo twig_escape_filter($this->env, $this->getAttribute($context["question"], "point", array()), "html", null, true);
                echo " ;
}
";
            } elseif (($this->getAttribute(            // line 49
$context["question"], "type", array()) == "Choix unique")) {
                // line 50
                echo "var resultat = false;
var x = document.form.radio";
                // line 51
                echo twig_escape_filter($this->env, $this->getAttribute($context["question"], "id", array()), "html", null, true);
                echo ".value;                    
";
                // line 52
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["reponse"]) ? $context["reponse"] : $this->getContext($context, "reponse")));
                foreach ($context['_seq'] as $context["_key"] => $context["rep"]) {
                    // line 53
                    if (($this->getAttribute($context["question"], "id", array()) == $this->getAttribute($this->getAttribute($context["rep"], "idquestion", array()), "id", array()))) {
                        // line 54
                        if (($this->getAttribute($context["rep"], "reponsecorrectradio", array()) == true)) {
                            // line 55
                            echo "

if (x == \"";
                            // line 57
                            echo twig_escape_filter($this->env, $this->getAttribute($context["rep"], "reponsecorrecttext", array()), "html", null, true);
                            echo "\")
{
resultat = true;
}

";
                        }
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['rep'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 65
                echo "if (resultat == true)
{
score = score +";
                // line 67
                echo twig_escape_filter($this->env, $this->getAttribute($context["question"], "point", array()), "html", null, true);
                echo " ;
}
";
            } else {
                // line 70
                echo "var resultat = false;                                            
    ";
                // line 71
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["reponse"]) ? $context["reponse"] : $this->getContext($context, "reponse")));
                foreach ($context['_seq'] as $context["_key"] => $context["rep"]) {
                    // line 72
                    if (($this->getAttribute($context["question"], "id", array()) == $this->getAttribute($this->getAttribute($context["rep"], "idquestion", array()), "id", array()))) {
                        // line 73
                        echo "var x = document.form.inp";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["rep"], "id", array()), "html", null, true);
                        echo ".value.toString();
if (x == \"";
                        // line 74
                        echo twig_escape_filter($this->env, $this->getAttribute($context["rep"], "reponsecorrecttext", array()), "html", null, true);
                        echo "\")
{
resultat = true;
}
";
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['rep'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 80
                echo "if (resultat == true)
{
score = score +";
                // line 82
                echo twig_escape_filter($this->env, $this->getAttribute($context["question"], "point", array()), "html", null, true);
                echo " ;
}
";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['question'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 86
        echo "
window.document.form.text1.value = Math.floor((score / point) * 100);
window.document.form.text2.value =";
        // line 88
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["quiz"]) ? $context["quiz"] : $this->getContext($context, "quiz")), "id", array()), "html", null, true);
        echo " ;
window.document.form.text3.value = 1;
}
</script>

        <!-- Page title -->
        <div class=\"page_top_wrap page_top_title page_top_breadcrumbs sc_pt_st1\">
            <div class=\"content_wrap\">
                <div class=\"breadcrumbs\">
                </div>
                <h1 class=\"page_title\"> <div id=\"status\" ></div>
                    <script>countDown(";
        // line 99
        echo twig_escape_filter($this->env, ($this->getAttribute((isset($context["quiz"]) ? $context["quiz"] : $this->getContext($context, "quiz")), "time", array()) * 60), "html", null, true);
        echo ", \"status\");</script></h1>
            </div>
        </div>
        <!-- /Page title -->
        <!-- Content with sidebar -->
        <div class=\"page_content_wrap\">
            <div class=\"content_wrap\">
                <!-- Content -->
                <div class=\"content\">

                    <article class=\"post_item post_item_single page\">\t\t\t\t\t\t\t
                        <section class=\"post_content\">

                            <!-- Quotes -->
                            <h3> ";
        // line 113
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["quiz"]) ? $context["quiz"] : $this->getContext($context, "quiz")), "quiznom", array()), "html", null, true);
        echo " <span> Duree ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["quiz"]) ? $context["quiz"] : $this->getContext($context, "quiz")), "time", array()), "html", null, true);
        echo " </span> </h3>
                            <blockquote cite=\"#\" class=\"sc_quote\">
                                <p>";
        // line 115
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["quiz"]) ? $context["quiz"] : $this->getContext($context, "quiz")), "quizdescription", array()), "html", null, true);
        echo "</p>
                            </blockquote>
                            <div class=\"sc_line sc_line_style_solid\"></div>
                            <!-- /Quotes -->
                            <!-- Accordion section -->
                            <div class=\"sc_section\" data-animation=\"animated fadeInUp normal\">
                                <form name=\"form\" method=\"POST\">

                                    ";
        // line 123
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["questions"]) ? $context["questions"] : $this->getContext($context, "questions")));
        foreach ($context['_seq'] as $context["_key"] => $context["question"]) {
            // line 124
            echo "                                        <div class=\"sc_accordion sc_accordion_style_1\" data-active=\"0\">

                                            <div class=\"sc_accordion_item odd first\">
                                                <h5 class=\"sc_accordion_title\">
                                                    <span class=\"sc_accordion_icon sc_accordion_icon_closed icon-plus-2\"></span>
                                                    <span class=\"sc_accordion_icon sc_accordion_icon_opened icon-minus-2\"></span>
                                                    ";
            // line 130
            echo twig_escape_filter($this->env, $this->getAttribute($context["question"], "questiontext", array()), "html", null, true);
            echo " <span> Ponit : ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["question"], "point", array()), "html", null, true);
            echo "</span>
                                                </h5>
                                                <div class=\"sc_accordion_content\">
                                                    <ul class=\"sc_list sc_list_style_iconed\">
                                                        <li class=\"sc_list_item odd first\">
                                                            <!-- hhhhhhhhhhhhhhhhhhh-->\t\t\t<span>
                                                                ";
            // line 136
            if (($this->getAttribute($context["question"], "type", array()) == "Choix multiple")) {
                // line 137
                echo "
                                                                    <form name=\"multiple\">
                                                                        <table class=\"records_list\">

                                                                            ";
                // line 141
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["reponse"]) ? $context["reponse"] : $this->getContext($context, "reponse")));
                foreach ($context['_seq'] as $context["_key"] => $context["rep"]) {
                    // line 142
                    echo "                                                                                ";
                    if (($this->getAttribute($context["question"], "id", array()) == $this->getAttribute($this->getAttribute($context["rep"], "idquestion", array()), "id", array()))) {
                        echo "    


                                                                                    <tr>
                                                                                        <td>
                                                                                        <input type=\"radio\" name=\"radio";
                        // line 147
                        echo twig_escape_filter($this->env, $this->getAttribute($context["rep"], "id", array()), "html", null, true);
                        echo "\" value=";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["rep"], "reponsecorrecttext", array()), "html", null, true);
                        echo ">
                                                                                        </td>
                                                                                        <td>  ";
                        // line 149
                        echo twig_escape_filter($this->env, $this->getAttribute($context["rep"], "reponsetext", array()), "html", null, true);
                        echo "</td>
                                                                                    </tr>




                                                                                ";
                    }
                    // line 156
                    echo "                                                                            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['rep'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 157
                echo "                                                                        </table>
                                                                    </form>
                                                                ";
            } elseif (($this->getAttribute(            // line 159
$context["question"], "type", array()) == "Choix unique")) {
                // line 160
                echo "
                                                                    <table class=\"records_list\">

                                                                        ";
                // line 163
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["reponse"]) ? $context["reponse"] : $this->getContext($context, "reponse")));
                foreach ($context['_seq'] as $context["_key"] => $context["rep"]) {
                    // line 164
                    echo "                                                                            ";
                    if (($this->getAttribute($context["question"], "id", array()) == $this->getAttribute($this->getAttribute($context["rep"], "idquestion", array()), "id", array()))) {
                        echo "    


                                                                                <tr>
                                                                                    <td>
                                                                                        <input type=\"radio\" name=\"radio";
                        // line 169
                        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["rep"], "idquestion", array()), "id", array()), "html", null, true);
                        echo "\" value=";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["rep"], "reponsecorrecttext", array()), "html", null, true);
                        echo ">
                                                                                    </td>

                                                                                    <td>  ";
                        // line 172
                        echo twig_escape_filter($this->env, $this->getAttribute($context["rep"], "reponsetext", array()), "html", null, true);
                        echo "</td>
                                                                                </tr>





                                                                            ";
                    }
                    // line 180
                    echo "                                                                        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['rep'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 181
                echo "                                                                    </table>

                                                                ";
            } else {
                // line 184
                echo "
                                                                    <table class=\"records_list\">

                                                                        ";
                // line 187
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["reponse"]) ? $context["reponse"] : $this->getContext($context, "reponse")));
                foreach ($context['_seq'] as $context["_key"] => $context["rep"]) {
                    // line 188
                    echo "                                                                            ";
                    if (($this->getAttribute($context["question"], "id", array()) == $this->getAttribute($this->getAttribute($context["rep"], "idquestion", array()), "id", array()))) {
                        echo "    



                                                                                <tr>
                                                                                    <td>  ";
                        // line 193
                        echo twig_escape_filter($this->env, $this->getAttribute($context["rep"], "reponsetext", array()), "html", null, true);
                        echo " :</td>
                                                                                    <td><input type=\"text\" name=\"inp";
                        // line 194
                        echo twig_escape_filter($this->env, $this->getAttribute($context["rep"], "id", array()), "html", null, true);
                        echo "\"></td>

                                                                                </tr>




                                                                            ";
                    }
                    // line 202
                    echo "                                                                        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['rep'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 203
                echo "                                                                    </table>
                                                            </li>

                                                        </ul>
                                                    </div>
                                                ";
            }
            // line 209
            echo "                                                <!-- hhhhhhhhhhhhhhhhhhh-->\t\t\t</span>

                                            </div>

                                        </div>

                                </div>
                                <!-- /Accordion section -->




                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['question'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 222
        echo "                            <input type=\"hidden\" name=\"text1\" value=\"\">
                            <input type=\"hidden\" name=\"text2\" value=\"\">
                            <input type=\"hidden\" name=\"text3\" value=\"\">
                            <input type=\"hidden\" name=\"text4\" value=\"\">

                            <input type=\"submit\" value=\"Terminer\" onclick=\"checkVal(this.form)\">
                            </form>



                        </section>
                    </article>
                </div>
                <!-- /Content -->

            </div>
        </div>
        <!-- /Content with sidebar -->
        ";
    }

    public function getTemplateName()
    {
        return "MoocQuizBundle:PasserQuiz:QuizFinal.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  432 => 222,  414 => 209,  406 => 203,  400 => 202,  389 => 194,  385 => 193,  376 => 188,  372 => 187,  367 => 184,  362 => 181,  356 => 180,  345 => 172,  337 => 169,  328 => 164,  324 => 163,  319 => 160,  317 => 159,  313 => 157,  307 => 156,  297 => 149,  290 => 147,  281 => 142,  277 => 141,  271 => 137,  269 => 136,  258 => 130,  250 => 124,  246 => 123,  235 => 115,  228 => 113,  211 => 99,  197 => 88,  193 => 86,  183 => 82,  179 => 80,  167 => 74,  162 => 73,  160 => 72,  156 => 71,  153 => 70,  147 => 67,  143 => 65,  129 => 57,  125 => 55,  123 => 54,  121 => 53,  117 => 52,  113 => 51,  110 => 50,  108 => 49,  103 => 47,  99 => 45,  85 => 37,  81 => 35,  79 => 34,  77 => 33,  73 => 32,  69 => 31,  65 => 29,  63 => 28,  58 => 27,  54 => 26,  31 => 5,  28 => 3,  11 => 1,);
    }
}
/* {% extends 'MoocQuizBundle::layout.html.twig' %}*/
/* */
/* {% block body -%}*/
/* */
/*     <script>*/
/*         function countDown(secs, elem) {*/
/*         var element = document.getElementById(elem);*/
/*                 element.innerHTML = "Il vous reste : " + Math.floor(secs / 60) + " minutes et  " + secs % 60 + " seconde ";*/
/*                 if (secs <= 1) {*/
/*         clearTimeout(timer);*/
/*                 window.document.form.text4.value = "timer";*/
/*                 element.innerHTML = checkVal(this.form);*/
/*                 window.document.form.submit();*/
/*         }*/
/*         secs--;*/
/*                 var timer = setTimeout('countDown(' + secs + ',"' + elem + '")', 1000);*/
/*         }*/
/*     </script>*/
/* */
/* */
/* */
/* */
/* <script  language="javascript">*/
/* function checkVal(form) {*/
/* var point = 0;*/
/* var score = 0;{% for question in questions %}*/
/* point = point +{{question.point}} ; */
/*            {% if question.type=='Choix multiple'%}*/
/* */
/* var resultat = false;*/
/* var x = document.form.radio{{reponse.id}}.value;                    */
/* {% for rep in reponse %}*/
/* {% if question.id == rep.idquestion.id %}*/
/* {% if rep.reponsecorrectradio == true %}*/
/* */
/* */
/* if (x == "{{rep.reponsecorrecttext}}")*/
/* {*/
/* resultat = true;*/
/* }*/
/* */
/* {% endif %}*/
/* {% endif %}*/
/* {% endfor %}*/
/* if (resultat == true)*/
/* {*/
/* score = score +{{question.point}} ;*/
/* }*/
/* {% elseif question.type=='Choix unique' %}*/
/* var resultat = false;*/
/* var x = document.form.radio{{question.id}}.value;                    */
/* {% for rep in reponse %}*/
/* {% if question.id == rep.idquestion.id %}*/
/* {% if rep.reponsecorrectradio == true %}*/
/* */
/* */
/* if (x == "{{rep.reponsecorrecttext}}")*/
/* {*/
/* resultat = true;*/
/* }*/
/* */
/* {% endif %}*/
/* {% endif %}*/
/* {% endfor %}*/
/* if (resultat == true)*/
/* {*/
/* score = score +{{question.point}} ;*/
/* }*/
/* {% else %}*/
/* var resultat = false;                                            */
/*     {% for rep in reponse %}*/
/* {% if question.id == rep.idquestion.id %}*/
/* var x = document.form.inp{{rep.id}}.value.toString();*/
/* if (x == "{{rep.reponsecorrecttext}}")*/
/* {*/
/* resultat = true;*/
/* }*/
/* {% endif %}*/
/* {% endfor %}*/
/* if (resultat == true)*/
/* {*/
/* score = score +{{question.point}} ;*/
/* }*/
/* {% endif %}*/
/* {% endfor %}*/
/* */
/* window.document.form.text1.value = Math.floor((score / point) * 100);*/
/* window.document.form.text2.value ={{quiz.id}} ;*/
/* window.document.form.text3.value = 1;*/
/* }*/
/* </script>*/
/* */
/*         <!-- Page title -->*/
/*         <div class="page_top_wrap page_top_title page_top_breadcrumbs sc_pt_st1">*/
/*             <div class="content_wrap">*/
/*                 <div class="breadcrumbs">*/
/*                 </div>*/
/*                 <h1 class="page_title"> <div id="status" ></div>*/
/*                     <script>countDown({{quiz.time*60}}, "status");</script></h1>*/
/*             </div>*/
/*         </div>*/
/*         <!-- /Page title -->*/
/*         <!-- Content with sidebar -->*/
/*         <div class="page_content_wrap">*/
/*             <div class="content_wrap">*/
/*                 <!-- Content -->*/
/*                 <div class="content">*/
/* */
/*                     <article class="post_item post_item_single page">							*/
/*                         <section class="post_content">*/
/* */
/*                             <!-- Quotes -->*/
/*                             <h3> {{ quiz.quiznom }} <span> Duree {{ quiz.time }} </span> </h3>*/
/*                             <blockquote cite="#" class="sc_quote">*/
/*                                 <p>{{ quiz.quizdescription }}</p>*/
/*                             </blockquote>*/
/*                             <div class="sc_line sc_line_style_solid"></div>*/
/*                             <!-- /Quotes -->*/
/*                             <!-- Accordion section -->*/
/*                             <div class="sc_section" data-animation="animated fadeInUp normal">*/
/*                                 <form name="form" method="POST">*/
/* */
/*                                     {% for question in questions %}*/
/*                                         <div class="sc_accordion sc_accordion_style_1" data-active="0">*/
/* */
/*                                             <div class="sc_accordion_item odd first">*/
/*                                                 <h5 class="sc_accordion_title">*/
/*                                                     <span class="sc_accordion_icon sc_accordion_icon_closed icon-plus-2"></span>*/
/*                                                     <span class="sc_accordion_icon sc_accordion_icon_opened icon-minus-2"></span>*/
/*                                                     {{ question.questiontext }} <span> Ponit : {{ question.point }}</span>*/
/*                                                 </h5>*/
/*                                                 <div class="sc_accordion_content">*/
/*                                                     <ul class="sc_list sc_list_style_iconed">*/
/*                                                         <li class="sc_list_item odd first">*/
/*                                                             <!-- hhhhhhhhhhhhhhhhhhh-->			<span>*/
/*                                                                 {% if question.type=='Choix multiple'%}*/
/* */
/*                                                                     <form name="multiple">*/
/*                                                                         <table class="records_list">*/
/* */
/*                                                                             {% for rep in reponse %}*/
/*                                                                                 {% if question.id == rep.idquestion.id %}    */
/* */
/* */
/*                                                                                     <tr>*/
/*                                                                                         <td>*/
/*                                                                                         <input type="radio" name="radio{{rep.id}}" value={{rep.reponsecorrecttext}}>*/
/*                                                                                         </td>*/
/*                                                                                         <td>  {{ rep.reponsetext }}</td>*/
/*                                                                                     </tr>*/
/* */
/* */
/* */
/* */
/*                                                                                 {% endif %}*/
/*                                                                             {% endfor %}*/
/*                                                                         </table>*/
/*                                                                     </form>*/
/*                                                                 {% elseif question.type=='Choix unique' %}*/
/* */
/*                                                                     <table class="records_list">*/
/* */
/*                                                                         {% for rep in reponse %}*/
/*                                                                             {% if question.id == rep.idquestion.id %}    */
/* */
/* */
/*                                                                                 <tr>*/
/*                                                                                     <td>*/
/*                                                                                         <input type="radio" name="radio{{rep.idquestion.id}}" value={{rep.reponsecorrecttext}}>*/
/*                                                                                     </td>*/
/* */
/*                                                                                     <td>  {{ rep.reponsetext }}</td>*/
/*                                                                                 </tr>*/
/* */
/* */
/* */
/* */
/* */
/*                                                                             {% endif %}*/
/*                                                                         {% endfor %}*/
/*                                                                     </table>*/
/* */
/*                                                                 {% else %}*/
/* */
/*                                                                     <table class="records_list">*/
/* */
/*                                                                         {% for rep in reponse %}*/
/*                                                                             {% if question.id == rep.idquestion.id %}    */
/* */
/* */
/* */
/*                                                                                 <tr>*/
/*                                                                                     <td>  {{ rep.reponsetext }} :</td>*/
/*                                                                                     <td><input type="text" name="inp{{rep.id}}"></td>*/
/* */
/*                                                                                 </tr>*/
/* */
/* */
/* */
/* */
/*                                                                             {% endif %}*/
/*                                                                         {% endfor %}*/
/*                                                                     </table>*/
/*                                                             </li>*/
/* */
/*                                                         </ul>*/
/*                                                     </div>*/
/*                                                 {% endif %}*/
/*                                                 <!-- hhhhhhhhhhhhhhhhhhh-->			</span>*/
/* */
/*                                             </div>*/
/* */
/*                                         </div>*/
/* */
/*                                 </div>*/
/*                                 <!-- /Accordion section -->*/
/* */
/* */
/* */
/* */
/*                             {% endfor %}*/
/*                             <input type="hidden" name="text1" value="">*/
/*                             <input type="hidden" name="text2" value="">*/
/*                             <input type="hidden" name="text3" value="">*/
/*                             <input type="hidden" name="text4" value="">*/
/* */
/*                             <input type="submit" value="Terminer" onclick="checkVal(this.form)">*/
/*                             </form>*/
/* */
/* */
/* */
/*                         </section>*/
/*                     </article>*/
/*                 </div>*/
/*                 <!-- /Content -->*/
/* */
/*             </div>*/
/*         </div>*/
/*         <!-- /Content with sidebar -->*/
/*         {% endblock %}*/
/* */
