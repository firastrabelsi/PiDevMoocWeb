<?php

/* MoocBundle:Registration:formulaireorganisme1.html.twig */
class __TwigTemplate_7e8debb48e8abc63cd8d43e1d93961336f84bb93acaebc0d468e78b058bef61f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("MoocBundle::layout.html.twig", "MoocBundle:Registration:formulaireorganisme1.html.twig", 2);
        $this->blocks = array(
            'formulaire' => array($this, 'block_formulaire'),
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "MoocBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_formulaire($context, array $blocks = array())
    {
        // line 4
        $this->displayBlock('fos_user_content', $context, $blocks);
    }

    public function block_fos_user_content($context, array $blocks = array())
    {
        // line 5
        echo "<h1> ";
        echo twig_escape_filter($this->env, (isset($context["email"]) ? $context["email"] : $this->getContext($context, "email")), "html", null, true);
        echo " </H1>
<form action=\"";
        // line 6
        echo $this->env->getExtension('routing')->getPath("registerstep2org");
        echo "\" methode=\"POST\">
    <table>
        <tr>
            <td> <label>Nom de la societe :</label></td>
            <td><input type=\"text\" name=\"nom\"/></td>
        </tr>
       
        <tr>
            <td> <label>Adresse :</label></td>
            <td><input type=\"text\" name=\"adresse\"/></td>
        </tr>
        


        <tr>
            <td>    <input type=\"hidden\" name=\"login\" value=\"";
        // line 21
        echo twig_escape_filter($this->env, (isset($context["email"]) ? $context["email"] : $this->getContext($context, "email")), "html", null, true);
        echo "\"/>
</td>
            <td><input type=\"submit\" value=\"Valider\"/></td>
        </tr>
    </table>
</form>
";
    }

    public function getTemplateName()
    {
        return "MoocBundle:Registration:formulaireorganisme1.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  61 => 21,  43 => 6,  38 => 5,  32 => 4,  29 => 3,  11 => 2,);
    }
}
/* {# empty Twig template #}*/
/* {% extends "MoocBundle::layout.html.twig" %}*/
/* {% block formulaire %}*/
/* {% block fos_user_content %}*/
/* <h1> {{email}} </H1>*/
/* <form action="{{path('registerstep2org')}}" methode="POST">*/
/*     <table>*/
/*         <tr>*/
/*             <td> <label>Nom de la societe :</label></td>*/
/*             <td><input type="text" name="nom"/></td>*/
/*         </tr>*/
/*        */
/*         <tr>*/
/*             <td> <label>Adresse :</label></td>*/
/*             <td><input type="text" name="adresse"/></td>*/
/*         </tr>*/
/*         */
/* */
/* */
/*         <tr>*/
/*             <td>    <input type="hidden" name="login" value="{{email}}"/>*/
/* </td>*/
/*             <td><input type="submit" value="Valider"/></td>*/
/*         </tr>*/
/*     </table>*/
/* </form>*/
/* {% endblock fos_user_content %}*/
/* {% endblock formulaire %}*/
/* */
