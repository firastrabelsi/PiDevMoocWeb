<?php

/* MoocBundle:admin:afficherComptes.html.twig */
class __TwigTemplate_7b4ed13533541c8d5948622d9f6934e52aa32a601af1fdd75df74d53c77fa96d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("MoocBundle:user:tables.html.twig ", "MoocBundle:admin:afficherComptes.html.twig", 1);
        $this->blocks = array(
            'affichage' => array($this, 'block_affichage'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "MoocBundle:user:tables.html.twig ";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_affichage($context, array $blocks = array())
    {
        // line 3
        echo "    <div class=\"main_container\">

        <div class=\"row-fluid\">
            <div class=\"widget widget-padding span12\">
                <div class=\"widget-header\">
                    <i class=\"icon-group\"></i>
                    <h5>Utilisateur</h5>

                </div>  
                <div class=\"widget-body\">
                    <table  class=\"table table-striped table-bordered dataTable\">
                        <thead>
                            <tr>
                                <th>Nom d'utlisateur</th>
                                <th>Role</th>
                              
                                <th>adresse</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            ";
        // line 24
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["comptes"]) ? $context["comptes"] : $this->getContext($context, "comptes")));
        foreach ($context['_seq'] as $context["_key"] => $context["compte"]) {
            // line 25
            echo "                                ";
            if (($this->getAttribute($context["compte"], "roles", array()) != "ROLE_ADMIN")) {
                // line 26
                echo "                                    <tr>
                                        <td>";
                // line 27
                echo twig_escape_filter($this->env, $this->getAttribute($context["compte"], "username", array()), "html", null, true);
                echo "</td>
                                        ";
                // line 28
                if (($this->getAttribute($context["compte"], "roles", array()) == "ROLE_APPRENANT")) {
                    // line 29
                    echo "                                            <td>APPREANT</td>
                                        ";
                } elseif (($this->getAttribute($this->getAttribute(                // line 30
$context["compte"], "roles", array()), 1, array(), "array") == "ROLE_ORGANISME")) {
                    // line 31
                    echo "                                            <td>ORGANISME</td>
                                        ";
                }
                // line 33
                echo "                                     
                                        ";
                // line 34
                if (($this->getAttribute($context["compte"], "etat", array()) == "valide")) {
                    // line 35
                    echo "                                            <td><span class=\"label label-success\">Valide</span></td> 
                                        ";
                } elseif (($this->getAttribute(                // line 36
$context["compte"], "etat", array()) == "masquer")) {
                    // line 37
                    echo "                                            <td><span class=\"label label-important\"> Banni</span></td>
                                        ";
                } else {
                    // line 39
                    echo "                                            <td><span class=\"label\">Inactive</span></td>   
                                        ";
                }
                // line 41
                echo "
                                        ";
                // line 42
                if (($this->getAttribute($context["compte"], "etat", array()) == "valide")) {
                    // line 43
                    echo "                                            <td>
                                                <a href=\" ";
                    // line 44
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("affiche_detail_compte", array("id" => $this->getAttribute($context["compte"], "id", array()))), "html", null, true);
                    echo "\"><i class=\"icon-user\"style=\"margin-right: 10px;\" ></i></a>
                                                <a href=\"";
                    // line 45
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("choix_admin_compte", array("choix" => 2, "id" => $this->getAttribute($context["compte"], "id", array()))), "html", null, true);
                    echo "\"><i class=\"icon-remove\"style=\"margin-right: 10px;\" ></i></a>

                                            </td> 
                                        ";
                } elseif (($this->getAttribute(                // line 48
$context["compte"], "etat", array()) == "masquer")) {
                    // line 49
                    echo "                                            <td>
                                                <a href=\"";
                    // line 50
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("choix_admin_compte", array("choix" => 3, "id" => $this->getAttribute($context["compte"], "id", array()))), "html", null, true);
                    echo "\"><i class=\"icon-refresh\"style=\"margin-right: 10px;\" ></i></a>
                                            </td>
                                        ";
                } else {
                    // line 53
                    echo "                                            <td>
                                                <a href=\"";
                    // line 54
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("affiche_detail_compte", array("id" => $this->getAttribute($context["compte"], "id", array()))), "html", null, true);
                    echo "\"><i class=\"icon-user\"style=\"margin-right: 10px;\" ></i></a>
                                                <a href=\"";
                    // line 55
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("choix_admin_compte", array("choix" => 2, "id" => $this->getAttribute($context["compte"], "id", array()))), "html", null, true);
                    echo "\"><i class=\"icon-remove\"style=\"margin-right: 10px;\" ></i></a>
                                                <a href=\"";
                    // line 56
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("choix_admin_compte", array("choix" => 1, "id" => $this->getAttribute($context["compte"], "id", array()))), "html", null, true);
                    echo "\"><i class=\"icon-thumbs-up\"style=\"margin-right: 10px;\" ></i></a>
                                            </td>
                                             <td><a href=\"";
                    // line 58
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("supp_compte", array("id" => $this->getAttribute($context["compte"], "id", array()))), "html", null, true);
                    echo "}\" target=\"_blank\">supprimer</a></td>
                                        ";
                }
                // line 60
                echo "                                    </tr>
                                ";
            }
            // line 62
            echo "                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['compte'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 63
        echo "                        </tbody>
                    </table>
                </div> <!-- /widget-body -->
            </div> <!-- /widget -->
        </div> <!-- /row-fluid -->
        <div class=\"row-fluid\">
            <form action=\"";
        // line 69
        echo $this->env->getExtension('routing')->getPath("cher");
        echo "\" methode=\"Post\">
                <table>
                    <tr>
                        <td> <label>Login</label></td>
                        <td><input type=\"text\" name=\"login\"/></td>
                    </tr>

                    
                    <tr>
                        <td></td>
                        <td><input type=\"submit\" value=\"chercher\"/></td>
                    </tr>
                </table>
            </form>

        </div>

    </div>




";
    }

    public function getTemplateName()
    {
        return "MoocBundle:admin:afficherComptes.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  165 => 69,  157 => 63,  151 => 62,  147 => 60,  142 => 58,  137 => 56,  133 => 55,  129 => 54,  126 => 53,  120 => 50,  117 => 49,  115 => 48,  109 => 45,  105 => 44,  102 => 43,  100 => 42,  97 => 41,  93 => 39,  89 => 37,  87 => 36,  84 => 35,  82 => 34,  79 => 33,  75 => 31,  73 => 30,  70 => 29,  68 => 28,  64 => 27,  61 => 26,  58 => 25,  54 => 24,  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {% extends "MoocBundle:user:tables.html.twig " %}*/
/* {% block affichage %}*/
/*     <div class="main_container">*/
/* */
/*         <div class="row-fluid">*/
/*             <div class="widget widget-padding span12">*/
/*                 <div class="widget-header">*/
/*                     <i class="icon-group"></i>*/
/*                     <h5>Utilisateur</h5>*/
/* */
/*                 </div>  */
/*                 <div class="widget-body">*/
/*                     <table  class="table table-striped table-bordered dataTable">*/
/*                         <thead>*/
/*                             <tr>*/
/*                                 <th>Nom d'utlisateur</th>*/
/*                                 <th>Role</th>*/
/*                               */
/*                                 <th>adresse</th>*/
/*                                 <th></th>*/
/*                             </tr>*/
/*                         </thead>*/
/*                         <tbody>*/
/*                             {% for compte in comptes%}*/
/*                                 {% if compte.roles!='ROLE_ADMIN'%}*/
/*                                     <tr>*/
/*                                         <td>{{compte.username}}</td>*/
/*                                         {%if compte.roles=='ROLE_APPRENANT' %}*/
/*                                             <td>APPREANT</td>*/
/*                                         {% elseif compte.roles[1]=='ROLE_ORGANISME'  %}*/
/*                                             <td>ORGANISME</td>*/
/*                                         {%endif%}*/
/*                                      */
/*                                         {% if compte.etat=='valide'%}*/
/*                                             <td><span class="label label-success">Valide</span></td> */
/*                                         {%elseif compte.etat=='masquer'%}*/
/*                                             <td><span class="label label-important"> Banni</span></td>*/
/*                                         {%else %}*/
/*                                             <td><span class="label">Inactive</span></td>   */
/*                                         {%endif%}*/
/* */
/*                                         {% if compte.etat=='valide'%}*/
/*                                             <td>*/
/*                                                 <a href=" {{path('affiche_detail_compte',{'id':compte.id})}}"><i class="icon-user"style="margin-right: 10px;" ></i></a>*/
/*                                                 <a href="{{path('choix_admin_compte',{'choix':2,'id':compte.id})}}"><i class="icon-remove"style="margin-right: 10px;" ></i></a>*/
/* */
/*                                             </td> */
/*                                         {%elseif compte.etat=='masquer'%}*/
/*                                             <td>*/
/*                                                 <a href="{{path('choix_admin_compte',{'choix':3,'id':compte.id})}}"><i class="icon-refresh"style="margin-right: 10px;" ></i></a>*/
/*                                             </td>*/
/*                                         {%else %}*/
/*                                             <td>*/
/*                                                 <a href="{{path('affiche_detail_compte',{'id':compte.id})}}"><i class="icon-user"style="margin-right: 10px;" ></i></a>*/
/*                                                 <a href="{{path('choix_admin_compte',{'choix':2,'id':compte.id})}}"><i class="icon-remove"style="margin-right: 10px;" ></i></a>*/
/*                                                 <a href="{{path('choix_admin_compte',{'choix':1,'id':compte.id})}}"><i class="icon-thumbs-up"style="margin-right: 10px;" ></i></a>*/
/*                                             </td>*/
/*                                              <td><a href="{{path('supp_compte',{'id':compte.id})}}}" target="_blank">supprimer</a></td>*/
/*                                         {%endif%}*/
/*                                     </tr>*/
/*                                 {%endif%}*/
/*                             {% endfor %}*/
/*                         </tbody>*/
/*                     </table>*/
/*                 </div> <!-- /widget-body -->*/
/*             </div> <!-- /widget -->*/
/*         </div> <!-- /row-fluid -->*/
/*         <div class="row-fluid">*/
/*             <form action="{{path('cher')}}" methode="Post">*/
/*                 <table>*/
/*                     <tr>*/
/*                         <td> <label>Login</label></td>*/
/*                         <td><input type="text" name="login"/></td>*/
/*                     </tr>*/
/* */
/*                     */
/*                     <tr>*/
/*                         <td></td>*/
/*                         <td><input type="submit" value="chercher"/></td>*/
/*                     </tr>*/
/*                 </table>*/
/*             </form>*/
/* */
/*         </div>*/
/* */
/*     </div>*/
/* */
/* */
/* */
/* */
/* {%endblock%}*/
/* */
