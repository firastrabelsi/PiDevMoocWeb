<?php

/* MoocQuizBundle:PasserQuiz:QuizDentrainement.html.twig */
class __TwigTemplate_1cb396b20fe59efe9567ae3af9c94a2c1da16631aef928747a2b18b2fec512e2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("MoocQuizBundle::layout.html.twig", "MoocQuizBundle:PasserQuiz:QuizDentrainement.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "MoocQuizBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 10
        echo "<script  language=\"javascript\">
        function checkVal(form) {
        var point = 0;
                var score = 0;";
        // line 13
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["questions"]) ? $context["questions"] : $this->getContext($context, "questions")));
        foreach ($context['_seq'] as $context["_key"] => $context["question"]) {
            // line 14
            echo "                point = point +";
            echo twig_escape_filter($this->env, $this->getAttribute($context["question"], "point", array()), "html", null, true);
            echo " ;        ";
            if (($this->getAttribute($context["question"], "type", array()) == "Choix multiple")) {
                // line 15
                echo "                            var resultat = false;            ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["reponse"]) ? $context["reponse"] : $this->getContext($context, "reponse")));
                foreach ($context['_seq'] as $context["_key"] => $context["rep"]) {
                    // line 16
                    echo "                ";
                    if (($this->getAttribute($context["question"], "id", array()) == $this->getAttribute($this->getAttribute($context["rep"], "idquestion", array()), "id", array()))) {
                        // line 17
                        echo "                            if (document.form.chk";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["rep"], "id", array()), "html", null, true);
                        echo ".checked)
                    {
                    var x = document.form.chk";
                        // line 19
                        echo twig_escape_filter($this->env, $this->getAttribute($context["rep"], "id", array()), "html", null, true);
                        echo ".value;                    ";
                        if ($this->getAttribute($context["rep"], "reponsecorrectradio", array())) {
                            // line 20
                            echo "
                                resultat = true;
                    ";
                        } else {
                            // line 22
                            echo " 
                                resultat = false;                    ";
                        }
                        // line 24
                        echo "                                    }
                ";
                    }
                    // line 26
                    echo "            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['rep'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 27
                echo "                if (resultat == true)
                {
                score = score +";
                // line 29
                echo twig_escape_filter($this->env, $this->getAttribute($context["question"], "point", array()), "html", null, true);
                echo " ;
                }
        ";
            } elseif (($this->getAttribute(            // line 31
$context["question"], "type", array()) == "Choix unique")) {
                // line 32
                echo "            var resultat = false;
                    var x = document.form.radio";
                // line 33
                echo twig_escape_filter($this->env, $this->getAttribute($context["question"], "id", array()), "html", null, true);
                echo ".value;            ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["reponse"]) ? $context["reponse"] : $this->getContext($context, "reponse")));
                foreach ($context['_seq'] as $context["_key"] => $context["rep"]) {
                    // line 34
                    echo "                ";
                    if (($this->getAttribute($context["question"], "id", array()) == $this->getAttribute($this->getAttribute($context["rep"], "idquestion", array()), "id", array()))) {
                        // line 35
                        echo "                    ";
                        if (($this->getAttribute($context["rep"], "reponsecorrectradio", array()) == true)) {
                            // line 36
                            echo "

                                if (x == \"";
                            // line 38
                            echo twig_escape_filter($this->env, $this->getAttribute($context["rep"], "reponsecorrecttext", array()), "html", null, true);
                            echo "\")
                        {
                        resultat = true;
                        }

                    ";
                        }
                        // line 44
                        echo "                ";
                    }
                    // line 45
                    echo "            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['rep'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 46
                echo "                if (resultat == true)
                {
                score = score +";
                // line 48
                echo twig_escape_filter($this->env, $this->getAttribute($context["question"], "point", array()), "html", null, true);
                echo " ;
                }
        ";
            } else {
                // line 51
                echo "            var resultat = false;            ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["reponse"]) ? $context["reponse"] : $this->getContext($context, "reponse")));
                foreach ($context['_seq'] as $context["_key"] => $context["rep"]) {
                    // line 52
                    echo "                ";
                    if (($this->getAttribute($context["question"], "id", array()) == $this->getAttribute($this->getAttribute($context["rep"], "idquestion", array()), "id", array()))) {
                        // line 53
                        echo "                            var x = document.form.inp";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["rep"], "id", array()), "html", null, true);
                        echo ".value.toString();
                            if (x == \"";
                        // line 54
                        echo twig_escape_filter($this->env, $this->getAttribute($context["rep"], "reponsecorrecttext", array()), "html", null, true);
                        echo "\")
                    {
                    resultat = true;
                    }
                ";
                    }
                    // line 59
                    echo "            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['rep'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 60
                echo "                if (resultat == true)
                {
                score = score +";
                // line 62
                echo twig_escape_filter($this->env, $this->getAttribute($context["question"], "point", array()), "html", null, true);
                echo " ;
                }
        ";
            }
            // line 65
            echo "        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['question'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 66
        echo "
            window.document.form.text1.value = Math.floor((score / point) * 100);
                    window.document.form.text2.value =";
        // line 68
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["quiz"]) ? $context["quiz"] : $this->getContext($context, "quiz")), "id", array()), "html", null, true);
        echo " ;
                    window.document.form.text3.value = 1;
                    }
        </script>

        <!-- Page title -->
        <div class=\"page_top_wrap page_top_title page_top_breadcrumbs sc_pt_st1\">
            <div class=\"content_wrap\">
                <div class=\"breadcrumbs\">
                </div>
                <h1 class=\"page_title\"> Quiz d'entrainement </h1>
            </div>
        </div>
        <!-- /Page title -->
        <!-- Content with sidebar -->
        <div class=\"page_content_wrap\">
            <div class=\"content_wrap\">
                <!-- Content -->
                <div class=\"content\">

                    <article class=\"post_item post_item_single page\">\t\t\t\t\t\t\t
                        <section class=\"post_content\">

                            <!-- Quotes -->
                            <h3> ";
        // line 92
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["quiz"]) ? $context["quiz"] : $this->getContext($context, "quiz")), "quiznom", array()), "html", null, true);
        echo " <span> Duree ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["quiz"]) ? $context["quiz"] : $this->getContext($context, "quiz")), "time", array()), "html", null, true);
        echo " </span> </h3>
                            <blockquote cite=\"#\" class=\"sc_quote\">
                                <p>";
        // line 94
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["quiz"]) ? $context["quiz"] : $this->getContext($context, "quiz")), "quizdescription", array()), "html", null, true);
        echo "</p>
                            </blockquote>
                            <div class=\"sc_line sc_line_style_solid\"></div>
                            <!-- /Quotes -->
                            <!-- Accordion section -->
                            <div class=\"sc_section\" data-animation=\"animated fadeInUp normal\">
                                <form name=\"form\" method=\"POST\" action=\"";
        // line 100
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("mooc_passer_question", array("id" => $this->getAttribute((isset($context["quiz"]) ? $context["quiz"] : $this->getContext($context, "quiz")), "id", array()))), "html", null, true);
        echo "\">

                                    ";
        // line 102
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["questions"]) ? $context["questions"] : $this->getContext($context, "questions")));
        foreach ($context['_seq'] as $context["_key"] => $context["question"]) {
            // line 103
            echo "                                        <div class=\"sc_accordion sc_accordion_style_1\" data-active=\"0\">

                                            <div class=\"sc_accordion_item odd first\">
                                                <h5 class=\"sc_accordion_title\">
                                                    <span class=\"sc_accordion_icon sc_accordion_icon_closed icon-plus-2\"></span>
                                                    <span class=\"sc_accordion_icon sc_accordion_icon_opened icon-minus-2\"></span>
                                                    ";
            // line 109
            echo twig_escape_filter($this->env, $this->getAttribute($context["question"], "questiontext", array()), "html", null, true);
            echo " <span> Ponit : ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["question"], "point", array()), "html", null, true);
            echo "</span>
                                                </h5>
                                                <div class=\"sc_accordion_content\">
                                                    <ul class=\"sc_list sc_list_style_iconed\">
                                                        <li class=\"sc_list_item odd first\">
                                                            <!-- hhhhhhhhhhhhhhhhhhh-->\t\t\t
                                                            <span>
                                                                ";
            // line 116
            if (($this->getAttribute($context["question"], "type", array()) == "Choix multiple")) {
                // line 117
                echo "
                                                                        <table class=\"records_list\">

                                                                            ";
                // line 120
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["reponse"]) ? $context["reponse"] : $this->getContext($context, "reponse")));
                foreach ($context['_seq'] as $context["_key"] => $context["rep"]) {
                    // line 121
                    echo "                                                                                ";
                    if (($this->getAttribute($context["question"], "id", array()) == $this->getAttribute($this->getAttribute($context["rep"], "idquestion", array()), "id", array()))) {
                        echo "    


                                                                                    <tr>
                                                                                        <td>
                                                                                            <input type=\"checkbox\" name =\"chk\" value=";
                        // line 126
                        echo twig_escape_filter($this->env, $this->getAttribute($context["rep"], "reponsecorrectradio", array()), "html", null, true);
                        echo " default=\"0\">
                                                                                        </td>
                                                                                        <td>  ";
                        // line 128
                        echo twig_escape_filter($this->env, $this->getAttribute($context["rep"], "reponsetext", array()), "html", null, true);
                        echo "</td>
                                                                                    </tr>




                                                                                ";
                    }
                    // line 135
                    echo "                                                                            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['rep'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 136
                echo "                                                                        </table>
                                                                ";
            } elseif (($this->getAttribute(            // line 137
$context["question"], "type", array()) == "Choix unique")) {
                // line 138
                echo "
                                                                    <table class=\"records_list\">

                                                                        ";
                // line 141
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["reponse"]) ? $context["reponse"] : $this->getContext($context, "reponse")));
                foreach ($context['_seq'] as $context["_key"] => $context["rep"]) {
                    // line 142
                    echo "                                                                            ";
                    if (($this->getAttribute($context["question"], "id", array()) == $this->getAttribute($this->getAttribute($context["rep"], "idquestion", array()), "id", array()))) {
                        echo "    


                                                                                <tr>
                                                                                    <td>
                                                                                        <input type=\"radio\" name=\"radio";
                        // line 147
                        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["rep"], "idquestion", array()), "id", array()), "html", null, true);
                        echo "\" value=";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["rep"], "reponsecorrecttext", array()), "html", null, true);
                        echo ">
                                                                                    </td>

                                                                                    <td>  ";
                        // line 150
                        echo twig_escape_filter($this->env, $this->getAttribute($context["rep"], "reponsetext", array()), "html", null, true);
                        echo "</td>
                                                                                </tr>





                                                                            ";
                    }
                    // line 158
                    echo "                                                                        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['rep'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 159
                echo "                                                                    </table>

                                                                ";
            } else {
                // line 162
                echo "
                                                                    <table class=\"records_list\">

                                                                        ";
                // line 165
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["reponse"]) ? $context["reponse"] : $this->getContext($context, "reponse")));
                foreach ($context['_seq'] as $context["_key"] => $context["rep"]) {
                    // line 166
                    echo "                                                                            ";
                    if (($this->getAttribute($context["question"], "id", array()) == $this->getAttribute($this->getAttribute($context["rep"], "idquestion", array()), "id", array()))) {
                        echo "    



                                                                                <tr>
                                                                                    <td>  ";
                        // line 171
                        echo twig_escape_filter($this->env, $this->getAttribute($context["rep"], "reponsetext", array()), "html", null, true);
                        echo " :</td>
                                                                                    <td><input type=\"text\" name=\"inp";
                        // line 172
                        echo twig_escape_filter($this->env, $this->getAttribute($context["rep"], "id", array()), "html", null, true);
                        echo "\"></td>

                                                                                </tr>




                                                                            ";
                    }
                    // line 180
                    echo "                                                                        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['rep'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 181
                echo "                                                                    </table>
                                                            </li>

                                                        </ul>
                                                    </div>
                                                ";
            }
            // line 187
            echo "                                                <!-- hhhhhhhhhhhhhhhhhhh-->\t\t\t</span>

                                            </div>

                                        </div>

                                </div>
                                <!-- /Accordion section -->




                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['question'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 200
        echo "                            <input type=\"hidden\" name=\"text1\" value=\"\">
                            <input type=\"hidden\" name=\"text2\" value=\"\">
                            <input type=\"hidden\" name=\"text3\" value=\"\">
                            <input type=\"hidden\" name=\"text4\" value=\"\">

                            <input type=\"submit\" value=\"Terminer\" onclick=\"checkVal(this.form)\">
                            </form>

                                </div>


                        </section>
                    </article>
                </div>
                <!-- /Content -->

            </div>
        </div>
        <!-- /Content with sidebar -->
        ";
    }

    public function getTemplateName()
    {
        return "MoocQuizBundle:PasserQuiz:QuizDentrainement.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  418 => 200,  400 => 187,  392 => 181,  386 => 180,  375 => 172,  371 => 171,  362 => 166,  358 => 165,  353 => 162,  348 => 159,  342 => 158,  331 => 150,  323 => 147,  314 => 142,  310 => 141,  305 => 138,  303 => 137,  300 => 136,  294 => 135,  284 => 128,  279 => 126,  270 => 121,  266 => 120,  261 => 117,  259 => 116,  247 => 109,  239 => 103,  235 => 102,  230 => 100,  221 => 94,  214 => 92,  187 => 68,  183 => 66,  177 => 65,  171 => 62,  167 => 60,  161 => 59,  153 => 54,  148 => 53,  145 => 52,  140 => 51,  134 => 48,  130 => 46,  124 => 45,  121 => 44,  112 => 38,  108 => 36,  105 => 35,  102 => 34,  96 => 33,  93 => 32,  91 => 31,  86 => 29,  82 => 27,  76 => 26,  72 => 24,  68 => 22,  63 => 20,  59 => 19,  53 => 17,  50 => 16,  45 => 15,  40 => 14,  36 => 13,  31 => 10,  28 => 3,  11 => 1,);
    }
}
/* {% extends 'MoocQuizBundle::layout.html.twig' %}*/
/* */
/* {% block body -%}*/
/* */
/* */
/* */
/* */
/* */
/* */
/*     <script  language="javascript">*/
/*         function checkVal(form) {*/
/*         var point = 0;*/
/*                 var score = 0;{% for question in questions %}*/
/*                 point = point +{{question.point}} ;        {% if question.type=='Choix multiple'%}*/
/*                             var resultat = false;            {% for rep in reponse %}*/
/*                 {% if question.id == rep.idquestion.id %}*/
/*                             if (document.form.chk{{rep.id}}.checked)*/
/*                     {*/
/*                     var x = document.form.chk{{rep.id}}.value;                    {% if rep.reponsecorrectradio %}*/
/* */
/*                                 resultat = true;*/
/*                     {% else %} */
/*                                 resultat = false;                    {% endif %}*/
/*                                     }*/
/*                 {% endif %}*/
/*             {% endfor %}*/
/*                 if (resultat == true)*/
/*                 {*/
/*                 score = score +{{question.point}} ;*/
/*                 }*/
/*         {% elseif question.type=='Choix unique' %}*/
/*             var resultat = false;*/
/*                     var x = document.form.radio{{question.id}}.value;            {% for rep in reponse %}*/
/*                 {% if question.id == rep.idquestion.id %}*/
/*                     {% if rep.reponsecorrectradio == true %}*/
/* */
/* */
/*                                 if (x == "{{rep.reponsecorrecttext}}")*/
/*                         {*/
/*                         resultat = true;*/
/*                         }*/
/* */
/*                     {% endif %}*/
/*                 {% endif %}*/
/*             {% endfor %}*/
/*                 if (resultat == true)*/
/*                 {*/
/*                 score = score +{{question.point}} ;*/
/*                 }*/
/*         {% else %}*/
/*             var resultat = false;            {% for rep in reponse %}*/
/*                 {% if question.id == rep.idquestion.id %}*/
/*                             var x = document.form.inp{{rep.id}}.value.toString();*/
/*                             if (x == "{{rep.reponsecorrecttext}}")*/
/*                     {*/
/*                     resultat = true;*/
/*                     }*/
/*                 {% endif %}*/
/*             {% endfor %}*/
/*                 if (resultat == true)*/
/*                 {*/
/*                 score = score +{{question.point}} ;*/
/*                 }*/
/*         {% endif %}*/
/*         {% endfor %}*/
/* */
/*             window.document.form.text1.value = Math.floor((score / point) * 100);*/
/*                     window.document.form.text2.value ={{quiz.id}} ;*/
/*                     window.document.form.text3.value = 1;*/
/*                     }*/
/*         </script>*/
/* */
/*         <!-- Page title -->*/
/*         <div class="page_top_wrap page_top_title page_top_breadcrumbs sc_pt_st1">*/
/*             <div class="content_wrap">*/
/*                 <div class="breadcrumbs">*/
/*                 </div>*/
/*                 <h1 class="page_title"> Quiz d'entrainement </h1>*/
/*             </div>*/
/*         </div>*/
/*         <!-- /Page title -->*/
/*         <!-- Content with sidebar -->*/
/*         <div class="page_content_wrap">*/
/*             <div class="content_wrap">*/
/*                 <!-- Content -->*/
/*                 <div class="content">*/
/* */
/*                     <article class="post_item post_item_single page">							*/
/*                         <section class="post_content">*/
/* */
/*                             <!-- Quotes -->*/
/*                             <h3> {{ quiz.quiznom }} <span> Duree {{ quiz.time }} </span> </h3>*/
/*                             <blockquote cite="#" class="sc_quote">*/
/*                                 <p>{{ quiz.quizdescription }}</p>*/
/*                             </blockquote>*/
/*                             <div class="sc_line sc_line_style_solid"></div>*/
/*                             <!-- /Quotes -->*/
/*                             <!-- Accordion section -->*/
/*                             <div class="sc_section" data-animation="animated fadeInUp normal">*/
/*                                 <form name="form" method="POST" action="{{ path('mooc_passer_question', { 'id': quiz.id }) }}">*/
/* */
/*                                     {% for question in questions %}*/
/*                                         <div class="sc_accordion sc_accordion_style_1" data-active="0">*/
/* */
/*                                             <div class="sc_accordion_item odd first">*/
/*                                                 <h5 class="sc_accordion_title">*/
/*                                                     <span class="sc_accordion_icon sc_accordion_icon_closed icon-plus-2"></span>*/
/*                                                     <span class="sc_accordion_icon sc_accordion_icon_opened icon-minus-2"></span>*/
/*                                                     {{ question.questiontext }} <span> Ponit : {{ question.point }}</span>*/
/*                                                 </h5>*/
/*                                                 <div class="sc_accordion_content">*/
/*                                                     <ul class="sc_list sc_list_style_iconed">*/
/*                                                         <li class="sc_list_item odd first">*/
/*                                                             <!-- hhhhhhhhhhhhhhhhhhh-->			*/
/*                                                             <span>*/
/*                                                                 {% if question.type=='Choix multiple'%}*/
/* */
/*                                                                         <table class="records_list">*/
/* */
/*                                                                             {% for rep in reponse %}*/
/*                                                                                 {% if question.id == rep.idquestion.id %}    */
/* */
/* */
/*                                                                                     <tr>*/
/*                                                                                         <td>*/
/*                                                                                             <input type="checkbox" name ="chk" value={{rep.reponsecorrectradio}} default="0">*/
/*                                                                                         </td>*/
/*                                                                                         <td>  {{ rep.reponsetext }}</td>*/
/*                                                                                     </tr>*/
/* */
/* */
/* */
/* */
/*                                                                                 {% endif %}*/
/*                                                                             {% endfor %}*/
/*                                                                         </table>*/
/*                                                                 {% elseif question.type=='Choix unique' %}*/
/* */
/*                                                                     <table class="records_list">*/
/* */
/*                                                                         {% for rep in reponse %}*/
/*                                                                             {% if question.id == rep.idquestion.id %}    */
/* */
/* */
/*                                                                                 <tr>*/
/*                                                                                     <td>*/
/*                                                                                         <input type="radio" name="radio{{rep.idquestion.id}}" value={{rep.reponsecorrecttext}}>*/
/*                                                                                     </td>*/
/* */
/*                                                                                     <td>  {{ rep.reponsetext }}</td>*/
/*                                                                                 </tr>*/
/* */
/* */
/* */
/* */
/* */
/*                                                                             {% endif %}*/
/*                                                                         {% endfor %}*/
/*                                                                     </table>*/
/* */
/*                                                                 {% else %}*/
/* */
/*                                                                     <table class="records_list">*/
/* */
/*                                                                         {% for rep in reponse %}*/
/*                                                                             {% if question.id == rep.idquestion.id %}    */
/* */
/* */
/* */
/*                                                                                 <tr>*/
/*                                                                                     <td>  {{ rep.reponsetext }} :</td>*/
/*                                                                                     <td><input type="text" name="inp{{rep.id}}"></td>*/
/* */
/*                                                                                 </tr>*/
/* */
/* */
/* */
/* */
/*                                                                             {% endif %}*/
/*                                                                         {% endfor %}*/
/*                                                                     </table>*/
/*                                                             </li>*/
/* */
/*                                                         </ul>*/
/*                                                     </div>*/
/*                                                 {% endif %}*/
/*                                                 <!-- hhhhhhhhhhhhhhhhhhh-->			</span>*/
/* */
/*                                             </div>*/
/* */
/*                                         </div>*/
/* */
/*                                 </div>*/
/*                                 <!-- /Accordion section -->*/
/* */
/* */
/* */
/* */
/*                             {% endfor %}*/
/*                             <input type="hidden" name="text1" value="">*/
/*                             <input type="hidden" name="text2" value="">*/
/*                             <input type="hidden" name="text3" value="">*/
/*                             <input type="hidden" name="text4" value="">*/
/* */
/*                             <input type="submit" value="Terminer" onclick="checkVal(this.form)">*/
/*                             </form>*/
/* */
/*                                 </div>*/
/* */
/* */
/*                         </section>*/
/*                     </article>*/
/*                 </div>*/
/*                 <!-- /Content -->*/
/* */
/*             </div>*/
/*         </div>*/
/*         <!-- /Content with sidebar -->*/
/*         {% endblock %}*/
/* */
