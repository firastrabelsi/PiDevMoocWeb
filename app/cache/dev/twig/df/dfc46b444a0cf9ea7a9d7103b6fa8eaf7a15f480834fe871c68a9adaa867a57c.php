<?php

/* MoocBundle:user:afficherOffres.html.twig */
class __TwigTemplate_bb70323c9a613f398963c46e80fdbcf25d3ab29bb12a06f912c4b4298ad150d0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("PiTdocBundle:user:dashbord.html.twig ", "MoocBundle:user:afficherOffres.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "PiTdocBundle:user:dashbord.html.twig ";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        // line 3
        echo "    <div class=\"main_container\">

        <div class=\"row-fluid\">
            <div class=\"widget widget-padding span12\">
                <div class=\"widget-header\">
                    <i class=\"icon-group\"></i>
                    <h5>Offres</h5>

                </div>  
                <div class=\"widget-body\">
                    <table  class=\"table table-striped table-bordered dataTable\">
                        <thead>
                            <tr>
                                <th>Client</th>
                                <th>Mode</th>
                                <th>Date debut</th>
                                <th>Langue source</th>
                                <th>Traducteur</th>
                                <th>status</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            ";
        // line 26
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["offres"]) ? $context["offres"] : $this->getContext($context, "offres")));
        foreach ($context['_seq'] as $context["_key"] => $context["offre"]) {
            // line 27
            echo "
                                <tr>
                                    <td>";
            // line 29
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["offre"], "client", array()), "username", array()), "html", null, true);
            echo "</td>
                                    <td>";
            // line 30
            echo twig_escape_filter($this->env, $this->getAttribute($context["offre"], "mode", array()), "html", null, true);
            echo "</td>

                                    <td>";
            // line 32
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["offre"], "dateDebut", array()), "D d/m/y"), "html", null, true);
            echo "</td>
                                    <td>";
            // line 33
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["offre"], "document", array()), "langueSrc", array()), "html", null, true);
            echo "</td>   
                                    <td>";
            // line 34
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["offre"], "traducteur", array()), "username", array()), "html", null, true);
            echo "</td>
                                    ";
            // line 35
            if (($this->getAttribute($context["offre"], "etat", array()) == "non valide")) {
                // line 36
                echo "                                        <td><span class=\"label label-danger\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["offre"], "etat", array()), "html", null, true);
                echo "</span></td>
                                        ";
            } elseif (($this->getAttribute(            // line 37
$context["offre"], "etat", array()) == "valide")) {
                // line 38
                echo "                                        <td><span class=\"label label-warning\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["offre"], "etat", array()), "html", null, true);
                echo "</span></td>
                                        ";
            } elseif (($this->getAttribute(            // line 39
$context["offre"], "etat", array()) == "en cours")) {
                // line 40
                echo "                                        <td><span class=\"label label-success\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["offre"], "etat", array()), "html", null, true);
                echo "</span></td>
                                        ";
            } elseif (($this->getAttribute(            // line 41
$context["offre"], "etat", array()) == "fini")) {
                // line 42
                echo "                                        <td><span class=\"label label-default\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["offre"], "etat", array()), "html", null, true);
                echo "</span></td>
                                        ";
            }
            // line 43
            echo "     

                                    ";
            // line 45
            if (($this->getAttribute($context["offre"], "etat", array()) == "non valide")) {
                // line 46
                echo "                                        <td>
                                            <a href=\"";
                // line 47
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("choix_admin_offre", array("choix" => 1, "id" => $this->getAttribute($context["offre"], "id", array()))), "html", null, true);
                echo "\"><i class=\"icon icon-thumbs-up\" style=\"margin-right: 10px\"></i></a>
                                            <a href=\"";
                // line 48
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("choix_admin_offre", array("choix" => 2, "id" => $this->getAttribute($context["offre"], "id", array()))), "html", null, true);
                echo "\"><i class=\"icon icon-remove\" style=\"margin-right: 10px\"></i></a>
                                            <a href=\"";
                // line 49
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("affiche_detail_offre", array("id" => $this->getAttribute($context["offre"], "id", array()))), "html", null, true);
                echo "\"><i class=\"icon icon-book\"></i></a>
                                        </td>
                                    ";
            } elseif (($this->getAttribute(            // line 51
$context["offre"], "etat", array()) == "valide")) {
                // line 52
                echo "                                        <td><a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("affiche_detail_offre", array("id" => $this->getAttribute($context["offre"], "id", array()))), "html", null, true);
                echo "\"><i class=\"icon icon-book\"></i></a>
                                            <a href=\"";
                // line 53
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("choix_admin_offre", array("choix" => 2, "id" => $this->getAttribute($context["offre"], "id", array()))), "html", null, true);
                echo "\"><i class=\"icon icon-remove\" style=\"margin-right: 10px\"></i></a>
                                        </td>
                                    ";
            } elseif (($this->getAttribute(            // line 55
$context["offre"], "etat", array()) == "en cours")) {
                // line 56
                echo "                                        <td> <a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("affiche_detail_offre", array("id" => $this->getAttribute($context["offre"], "id", array()))), "html", null, true);
                echo "\"><i class=\"icon icon-book\"></i></a></td>
                                            ";
            } elseif (($this->getAttribute(            // line 57
$context["offre"], "etat", array()) == "fini")) {
                // line 58
                echo "                                        <td> <a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("affiche_detail_offre", array("id" => $this->getAttribute($context["offre"], "id", array()))), "html", null, true);
                echo "\"><i class=\"icon icon-book\"></i></a></td>

                                    ";
            }
            // line 60
            echo " 

                                </tr>

                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['offre'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 64
        echo " 
                        </tbody>
                    </table>
                </div> <!-- /widget-body -->
            </div> <!-- /widget -->
        </div> <!-- /row-fluid -->
    </div>




";
    }

    public function getTemplateName()
    {
        return "MoocBundle:user:afficherOffres.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  174 => 64,  164 => 60,  157 => 58,  155 => 57,  150 => 56,  148 => 55,  143 => 53,  138 => 52,  136 => 51,  131 => 49,  127 => 48,  123 => 47,  120 => 46,  118 => 45,  114 => 43,  108 => 42,  106 => 41,  101 => 40,  99 => 39,  94 => 38,  92 => 37,  87 => 36,  85 => 35,  81 => 34,  77 => 33,  73 => 32,  68 => 30,  64 => 29,  60 => 27,  56 => 26,  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {% extends "PiTdocBundle:user:dashbord.html.twig " %}*/
/* {% block body %}*/
/*     <div class="main_container">*/
/* */
/*         <div class="row-fluid">*/
/*             <div class="widget widget-padding span12">*/
/*                 <div class="widget-header">*/
/*                     <i class="icon-group"></i>*/
/*                     <h5>Offres</h5>*/
/* */
/*                 </div>  */
/*                 <div class="widget-body">*/
/*                     <table  class="table table-striped table-bordered dataTable">*/
/*                         <thead>*/
/*                             <tr>*/
/*                                 <th>Client</th>*/
/*                                 <th>Mode</th>*/
/*                                 <th>Date debut</th>*/
/*                                 <th>Langue source</th>*/
/*                                 <th>Traducteur</th>*/
/*                                 <th>status</th>*/
/*                                 <th></th>*/
/*                             </tr>*/
/*                         </thead>*/
/*                         <tbody>*/
/*                             {% for offre in offres%}*/
/* */
/*                                 <tr>*/
/*                                     <td>{{offre.client.username}}</td>*/
/*                                     <td>{{offre.mode}}</td>*/
/* */
/*                                     <td>{{offre.dateDebut|date('D d/m/y')}}</td>*/
/*                                     <td>{{offre.document.langueSrc}}</td>   */
/*                                     <td>{{offre.traducteur.username}}</td>*/
/*                                     {% if offre.etat=='non valide'%}*/
/*                                         <td><span class="label label-danger">{{offre.etat}}</span></td>*/
/*                                         {% elseif offre.etat=='valide'%}*/
/*                                         <td><span class="label label-warning">{{offre.etat}}</span></td>*/
/*                                         {% elseif offre.etat=='en cours'%}*/
/*                                         <td><span class="label label-success">{{offre.etat}}</span></td>*/
/*                                         {% elseif offre.etat=='fini'%}*/
/*                                         <td><span class="label label-default">{{offre.etat}}</span></td>*/
/*                                         {%endif%}     */
/* */
/*                                     {% if offre.etat=='non valide'%}*/
/*                                         <td>*/
/*                                             <a href="{{path('choix_admin_offre',{'choix':1,'id':offre.id})}}"><i class="icon icon-thumbs-up" style="margin-right: 10px"></i></a>*/
/*                                             <a href="{{path('choix_admin_offre',{'choix':2,'id':offre.id})}}"><i class="icon icon-remove" style="margin-right: 10px"></i></a>*/
/*                                             <a href="{{path('affiche_detail_offre',{'id':offre.id})}}"><i class="icon icon-book"></i></a>*/
/*                                         </td>*/
/*                                     {% elseif offre.etat=='valide'%}*/
/*                                         <td><a href="{{path('affiche_detail_offre',{'id':offre.id})}}"><i class="icon icon-book"></i></a>*/
/*                                             <a href="{{path('choix_admin_offre',{'choix':2,'id':offre.id})}}"><i class="icon icon-remove" style="margin-right: 10px"></i></a>*/
/*                                         </td>*/
/*                                     {% elseif offre.etat=='en cours'%}*/
/*                                         <td> <a href="{{path('affiche_detail_offre',{'id':offre.id})}}"><i class="icon icon-book"></i></a></td>*/
/*                                             {% elseif offre.etat=='fini'%}*/
/*                                         <td> <a href="{{path('affiche_detail_offre',{'id':offre.id})}}"><i class="icon icon-book"></i></a></td>*/
/* */
/*                                     {%endif%} */
/* */
/*                                 </tr>*/
/* */
/*                             {% endfor %} */
/*                         </tbody>*/
/*                     </table>*/
/*                 </div> <!-- /widget-body -->*/
/*             </div> <!-- /widget -->*/
/*         </div> <!-- /row-fluid -->*/
/*     </div>*/
/* */
/* */
/* */
/* */
/* {%endblock%}*/
/* */
/* */
