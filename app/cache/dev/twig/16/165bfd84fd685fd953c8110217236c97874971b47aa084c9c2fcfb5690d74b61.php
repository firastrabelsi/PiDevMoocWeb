<?php

/* MoocBundle:Registration:formulaireorganisme2.html.twig */
class __TwigTemplate_a76fe3368883874f990d2f5e33cb24eb20cba36fb9c9fdfa35f7fce03fe3fd65 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("MoocBundle::layout.html.twig", "MoocBundle:Registration:formulaireorganisme2.html.twig", 2);
        $this->blocks = array(
            'formulaire' => array($this, 'block_formulaire'),
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "MoocBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_formulaire($context, array $blocks = array())
    {
        // line 4
        $this->displayBlock('fos_user_content', $context, $blocks);
    }

    public function block_fos_user_content($context, array $blocks = array())
    {
        // line 5
        echo "<form action=\"";
        echo $this->env->getExtension('routing')->getPath("register_step3org");
        echo "\" methode=\"POST\">
    <table>
        <tr>
            <td> <label>Certificat :</label></td>
            <td><input type=\"text\" name=\"certificat\"/></td>
        </tr>
       
        <tr>
            <td> <label>Site URL :</label></td>
            <td><input type=\"text\" name=\"site\"/></td>
        </tr>
        <tr>
            <td> <label>Numero TEL :</label></td>
            <td><input type=\"text\" name=\"num\"/></td>
        </tr>
        


        <tr>
            <td>    <input type=\"hidden\" name=\"login\" value=\"\"/>
</td>
            <td><input type=\"submit\" value=\"Valider\"/></td>
        </tr>
    </table>
</form>
";
    }

    public function getTemplateName()
    {
        return "MoocBundle:Registration:formulaireorganisme2.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  38 => 5,  32 => 4,  29 => 3,  11 => 2,);
    }
}
/* {# empty Twig template #}*/
/* {% extends "MoocBundle::layout.html.twig" %}*/
/* {% block formulaire %}*/
/* {% block fos_user_content %}*/
/* <form action="{{path('register_step3org')}}" methode="POST">*/
/*     <table>*/
/*         <tr>*/
/*             <td> <label>Certificat :</label></td>*/
/*             <td><input type="text" name="certificat"/></td>*/
/*         </tr>*/
/*        */
/*         <tr>*/
/*             <td> <label>Site URL :</label></td>*/
/*             <td><input type="text" name="site"/></td>*/
/*         </tr>*/
/*         <tr>*/
/*             <td> <label>Numero TEL :</label></td>*/
/*             <td><input type="text" name="num"/></td>*/
/*         </tr>*/
/*         */
/* */
/* */
/*         <tr>*/
/*             <td>    <input type="hidden" name="login" value=""/>*/
/* </td>*/
/*             <td><input type="submit" value="Valider"/></td>*/
/*         </tr>*/
/*     </table>*/
/* </form>*/
/* {% endblock fos_user_content %}*/
/* {% endblock formulaire %}*/
/* */
