<?php

/* MoocBundle:Default:test.html.twig */
class __TwigTemplate_dfa9c1a885f90466ac1bae1c71573025ad84316c999deb5bf0ed963aa63bdc70 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["comptes"]) ? $context["comptes"] : $this->getContext($context, "comptes")), 0, array(), "array"), "username", array()), "html", null, true);
    }

    public function getTemplateName()
    {
        return "MoocBundle:Default:test.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 6,);
    }
}
/* {# empty Twig template */
/*                             {% for compte in comptes%}*/
/*                                 {{compte.email}}*/
/*                             {% endfor %}*/
/* #}*/
/* {{comptes[0].username}}*/
