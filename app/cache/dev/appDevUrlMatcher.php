<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appDevUrlMatcher.
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appDevUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        if (0 === strpos($pathinfo, '/_')) {
            // _wdt
            if (0 === strpos($pathinfo, '/_wdt') && preg_match('#^/_wdt/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_wdt')), array (  '_controller' => 'web_profiler.controller.profiler:toolbarAction',));
            }

            if (0 === strpos($pathinfo, '/_profiler')) {
                // _profiler_home
                if (rtrim($pathinfo, '/') === '/_profiler') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_profiler_home');
                    }

                    return array (  '_controller' => 'web_profiler.controller.profiler:homeAction',  '_route' => '_profiler_home',);
                }

                if (0 === strpos($pathinfo, '/_profiler/search')) {
                    // _profiler_search
                    if ($pathinfo === '/_profiler/search') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchAction',  '_route' => '_profiler_search',);
                    }

                    // _profiler_search_bar
                    if ($pathinfo === '/_profiler/search_bar') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchBarAction',  '_route' => '_profiler_search_bar',);
                    }

                }

                // _profiler_purge
                if ($pathinfo === '/_profiler/purge') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:purgeAction',  '_route' => '_profiler_purge',);
                }

                // _profiler_info
                if (0 === strpos($pathinfo, '/_profiler/info') && preg_match('#^/_profiler/info/(?P<about>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_info')), array (  '_controller' => 'web_profiler.controller.profiler:infoAction',));
                }

                // _profiler_phpinfo
                if ($pathinfo === '/_profiler/phpinfo') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:phpinfoAction',  '_route' => '_profiler_phpinfo',);
                }

                // _profiler_search_results
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/search/results$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_search_results')), array (  '_controller' => 'web_profiler.controller.profiler:searchResultsAction',));
                }

                // _profiler
                if (preg_match('#^/_profiler/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler')), array (  '_controller' => 'web_profiler.controller.profiler:panelAction',));
                }

                // _profiler_router
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/router$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_router')), array (  '_controller' => 'web_profiler.controller.router:panelAction',));
                }

                // _profiler_exception
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception')), array (  '_controller' => 'web_profiler.controller.exception:showAction',));
                }

                // _profiler_exception_css
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception\\.css$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception_css')), array (  '_controller' => 'web_profiler.controller.exception:cssAction',));
                }

            }

            if (0 === strpos($pathinfo, '/_configurator')) {
                // _configurator_home
                if (rtrim($pathinfo, '/') === '/_configurator') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_configurator_home');
                    }

                    return array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::checkAction',  '_route' => '_configurator_home',);
                }

                // _configurator_step
                if (0 === strpos($pathinfo, '/_configurator/step') && preg_match('#^/_configurator/step/(?P<index>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_configurator_step')), array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::stepAction',));
                }

                // _configurator_final
                if ($pathinfo === '/_configurator/final') {
                    return array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::finalAction',  '_route' => '_configurator_final',);
                }

            }

            // _twig_error_test
            if (0 === strpos($pathinfo, '/_error') && preg_match('#^/_error/(?P<code>\\d+)(?:\\.(?P<_format>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_twig_error_test')), array (  '_controller' => 'twig.controller.preview_error:previewErrorPageAction',  '_format' => 'html',));
            }

        }

        // moo_ccommentaire_homepage
        if ($pathinfo === '/hello') {
            return array (  '_controller' => 'MOOC\\commentaireBundle\\Controller\\DefaultController::indexAction',  '_route' => 'moo_ccommentaire_homepage',);
        }

        if (0 === strpos($pathinfo, '/Commentaire')) {
            // Commentaire
            if (preg_match('#^/Commentaire/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'Commentaire')), array (  '_controller' => 'MOOCcommentaireBundle:Commentaire2:commenter',));
            }

            // Commentaireaff
            if (0 === strpos($pathinfo, '/Commentaireaff') && preg_match('#^/Commentaireaff/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'Commentaireaff')), array (  '_controller' => 'MOOCcommentaireBundle:Commentaire:ajout',));
            }

        }

        // vote
        if (0 === strpos($pathinfo, '/vote') && preg_match('#^/vote/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'vote')), array (  '_controller' => 'MOOCcommentaireBundle:Commentaire:rating',));
        }

        // supprimer
        if (0 === strpos($pathinfo, '/supprimer') && preg_match('#^/supprimer/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'supprimer')), array (  '_controller' => 'MOOCcommentaireBundle:Commentaire:delete',));
        }

        if (0 === strpos($pathinfo, '/hello')) {
            // pi_cour_list
            if ($pathinfo === '/hello') {
                return array (  '_controller' => 'MOOC\\commentaireBundle\\Controller\\DefaultController::indexAction',  '_route' => 'pi_cour_list',);
            }

            // pi_chapitre_list
            if ($pathinfo === '/hello') {
                return array (  '_controller' => 'MOOC\\commentaireBundle\\Controller\\DefaultController::indexAction',  '_route' => 'pi_chapitre_list',);
            }

            // pi_graphe
            if ($pathinfo === '/hello') {
                return array (  '_controller' => 'MOOC\\commentaireBundle\\Controller\\DefaultController::indexAction',  '_route' => 'pi_graphe',);
            }

        }

        if (0 === strpos($pathinfo, '/badge')) {
            // badge
            if ($pathinfo === '/badge') {
                return array (  '_controller' => 'MOOCcommentaireBundle:badge:upload',  '_route' => 'badge',);
            }

            if (0 === strpos($pathinfo, '/badgeUpload')) {
                // badgeUpload
                if (preg_match('#^/badgeUpload/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'badgeUpload')), array (  '_controller' => 'MOOCcommentaireBundle:badge:list',));
                }

                if (0 === strpos($pathinfo, '/badgeUploads')) {
                    // my_image_route
                    if (preg_match('#^/badgeUploads/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'my_image_route')), array (  '_controller' => 'MOOCcommentaireBundle:badge:photo',));
                    }

                    // my_image_route2
                    if (0 === strpos($pathinfo, '/badgeUploads2') && preg_match('#^/badgeUploads2/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'my_image_route2')), array (  '_controller' => 'MOOCcommentaireBundle:badge:photo2',));
                    }

                    // my_image_route3
                    if (0 === strpos($pathinfo, '/badgeUploads3') && preg_match('#^/badgeUploads3/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'my_image_route3')), array (  '_controller' => 'MOOCcommentaireBundle:badge:photo3',));
                    }

                }

            }

        }

        if (0 === strpos($pathinfo, '/reponse')) {
            // reponse
            if (rtrim($pathinfo, '/') === '/reponse') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'reponse');
                }

                return array (  '_controller' => 'Mooc\\QuizBundle\\Controller\\ReponseController::indexAction',  '_route' => 'reponse',);
            }

            // reponse_show
            if (preg_match('#^/reponse/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'reponse_show')), array (  '_controller' => 'Mooc\\QuizBundle\\Controller\\ReponseController::showAction',));
            }

            // reponse_new
            if (0 === strpos($pathinfo, '/reponse/new') && preg_match('#^/reponse/new/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'reponse_new')), array (  '_controller' => 'Mooc\\QuizBundle\\Controller\\ReponseController::newAction',));
            }

            // reponse_create
            if ($pathinfo === '/reponse/create') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_reponse_create;
                }

                return array (  '_controller' => 'Mooc\\QuizBundle\\Controller\\ReponseController::createAction',  '_route' => 'reponse_create',);
            }
            not_reponse_create:

            // reponse_edit
            if (preg_match('#^/reponse/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'reponse_edit')), array (  '_controller' => 'Mooc\\QuizBundle\\Controller\\ReponseController::editAction',));
            }

            // reponse_update
            if (preg_match('#^/reponse/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                    $allow = array_merge($allow, array('POST', 'PUT'));
                    goto not_reponse_update;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'reponse_update')), array (  '_controller' => 'Mooc\\QuizBundle\\Controller\\ReponseController::updateAction',));
            }
            not_reponse_update:

            // reponse_delete
            if (preg_match('#^/reponse/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                    $allow = array_merge($allow, array('POST', 'DELETE'));
                    goto not_reponse_delete;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'reponse_delete')), array (  '_controller' => 'Mooc\\QuizBundle\\Controller\\ReponseController::deleteAction',));
            }
            not_reponse_delete:

            // reponse_list
            if (0 === strpos($pathinfo, '/reponse/list') && preg_match('#^/reponse/list/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'reponse_list')), array (  '_controller' => 'Mooc\\QuizBundle\\Controller\\ReponseController::listAction',));
            }

        }

        if (0 === strpos($pathinfo, '/qu')) {
            if (0 === strpos($pathinfo, '/question')) {
                // question
                if (rtrim($pathinfo, '/') === '/question') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'question');
                    }

                    return array (  '_controller' => 'Mooc\\QuizBundle\\Controller\\QuestionController::indexAction',  '_route' => 'question',);
                }

                // question_show
                if (preg_match('#^/question/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'question_show')), array (  '_controller' => 'Mooc\\QuizBundle\\Controller\\QuestionController::showAction',));
                }

                // question_new
                if (0 === strpos($pathinfo, '/question/new') && preg_match('#^/question/new/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'question_new')), array (  '_controller' => 'Mooc\\QuizBundle\\Controller\\QuestionController::newAction',));
                }

                // question_create
                if ($pathinfo === '/question/create') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_question_create;
                    }

                    return array (  '_controller' => 'Mooc\\QuizBundle\\Controller\\QuestionController::createAction',  '_route' => 'question_create',);
                }
                not_question_create:

                // question_edit
                if (preg_match('#^/question/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'question_edit')), array (  '_controller' => 'Mooc\\QuizBundle\\Controller\\QuestionController::editAction',));
                }

                // question_update
                if (preg_match('#^/question/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                        $allow = array_merge($allow, array('POST', 'PUT'));
                        goto not_question_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'question_update')), array (  '_controller' => 'Mooc\\QuizBundle\\Controller\\QuestionController::updateAction',));
                }
                not_question_update:

                // question_delete
                if (preg_match('#^/question/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                        $allow = array_merge($allow, array('POST', 'DELETE'));
                        goto not_question_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'question_delete')), array (  '_controller' => 'Mooc\\QuizBundle\\Controller\\QuestionController::deleteAction',));
                }
                not_question_delete:

                // question_list
                if (0 === strpos($pathinfo, '/question/list') && preg_match('#^/question/list/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'question_list')), array (  '_controller' => 'Mooc\\QuizBundle\\Controller\\QuestionController::listAction',));
                }

            }

            if (0 === strpos($pathinfo, '/quiz')) {
                // quiz
                if (rtrim($pathinfo, '/') === '/quiz') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'quiz');
                    }

                    return array (  '_controller' => 'Mooc\\QuizBundle\\Controller\\QuizController::indexAction',  '_route' => 'quiz',);
                }

                // quiz_show
                if (preg_match('#^/quiz/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'quiz_show')), array (  '_controller' => 'Mooc\\QuizBundle\\Controller\\QuizController::showAction',));
                }

                // quiz_new
                if ($pathinfo === '/quiz/new') {
                    return array (  '_controller' => 'Mooc\\QuizBundle\\Controller\\QuizController::newAction',  '_route' => 'quiz_new',);
                }

                // quiz_create
                if ($pathinfo === '/quiz/create') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_quiz_create;
                    }

                    return array (  '_controller' => 'Mooc\\QuizBundle\\Controller\\QuizController::createAction',  '_route' => 'quiz_create',);
                }
                not_quiz_create:

                // quiz_edit
                if (preg_match('#^/quiz/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'quiz_edit')), array (  '_controller' => 'Mooc\\QuizBundle\\Controller\\QuizController::editAction',));
                }

                // quiz_update
                if (preg_match('#^/quiz/(?P<id>[^/]++)/update$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                        $allow = array_merge($allow, array('POST', 'PUT'));
                        goto not_quiz_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'quiz_update')), array (  '_controller' => 'Mooc\\QuizBundle\\Controller\\QuizController::updateAction',));
                }
                not_quiz_update:

                // quiz_delete
                if (preg_match('#^/quiz/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                        $allow = array_merge($allow, array('POST', 'DELETE'));
                        goto not_quiz_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'quiz_delete')), array (  '_controller' => 'Mooc\\QuizBundle\\Controller\\QuizController::deleteAction',));
                }
                not_quiz_delete:

                // quiz_list
                if (0 === strpos($pathinfo, '/quiz/list') && preg_match('#^/quiz/list/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'quiz_list')), array (  '_controller' => 'Mooc\\QuizBundle\\Controller\\QuizController::listAction',));
                }

            }

        }

        // mooc_quiz_homepage
        if (0 === strpos($pathinfo, '/hello') && preg_match('#^/hello/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'mooc_quiz_homepage')), array (  '_controller' => 'Mooc\\QuizBundle\\Controller\\DefaultController::indexAction',));
        }

        // mooc_passer_quiz
        if (0 === strpos($pathinfo, '/exam') && preg_match('#^/exam/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'mooc_passer_quiz')), array (  '_controller' => 'Mooc\\QuizBundle\\Controller\\PasserQuizController::examAction',));
        }

        if (0 === strpos($pathinfo, '/passerquestion')) {
            // mooc_passer_question
            if (preg_match('#^/passerquestion/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'mooc_passer_question')), array (  '_controller' => 'Mooc\\QuizBundle\\Controller\\PasserQuizController::questionAction',));
            }

            // mooc_passer_question_entrainement
            if (0 === strpos($pathinfo, '/passerquestiontest') && preg_match('#^/passerquestiontest/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'mooc_passer_question_entrainement')), array (  '_controller' => 'Mooc\\QuizBundle\\Controller\\PasserQuizController::examAction',));
            }

        }

        // pidevmooc_homepage
        if ($pathinfo === '/hello') {
            return array (  '_controller' => 'moocApp\\MoocBundle\\Controller\\DefaultController::indexAction',  '_route' => 'pidevmooc_homepage',);
        }

        // pidevmooc_aboutus
        if ($pathinfo === '/aboutus') {
            return array (  '_controller' => 'moocApp\\MoocBundle\\Controller\\DefaultController::aboutusAction',  '_route' => 'pidevmooc_aboutus',);
        }

        // pidevmooc_contactus
        if ($pathinfo === '/contactus') {
            return array (  '_controller' => 'moocApp\\MoocBundle\\Controller\\DefaultController::contactusAction',  '_route' => 'pidevmooc_contactus',);
        }

        // pidevmooc_inscription
        if ($pathinfo === '/inscription') {
            return array (  '_controller' => 'moocApp\\MoocBundle\\Controller\\DefaultController::inscriptionAction',  '_route' => 'pidevmooc_inscription',);
        }

        if (0 === strpos($pathinfo, '/step')) {
            if (0 === strpos($pathinfo, '/step2')) {
                // registerstep2
                if ($pathinfo === '/step2') {
                    return array (  '_controller' => 'moocApp\\MoocBundle\\Controller\\DefaultController::RegisterStep2Action',  '_route' => 'registerstep2',);
                }

                // registerstep2org
                if ($pathinfo === '/step2org') {
                    return array (  '_controller' => 'moocApp\\MoocBundle\\Controller\\DefaultController::RegisterStep2orgAction',  '_route' => 'registerstep2org',);
                }

            }

            // register_step3org
            if ($pathinfo === '/step3org') {
                return array (  '_controller' => 'moocApp\\MoocBundle\\Controller\\DefaultController::RegisterStep3orgAction',  '_route' => 'register_step3org',);
            }

        }

        // registerformstep3org
        if ($pathinfo === '/formstep3org') {
            return array (  '_controller' => 'moocApp\\MoocBundle\\Controller\\DefaultController::formstep3Action',  '_route' => 'registerformstep3org',);
        }

        // pidevmooc_profil
        if ($pathinfo === '/profil') {
            return array (  '_controller' => 'moocApp\\MoocBundle\\Controller\\DefaultController::profilAction',  '_route' => 'pidevmooc_profil',);
        }

        if (0 === strpos($pathinfo, '/a')) {
            if (0 === strpos($pathinfo, '/admin')) {
                // dashboard
                if ($pathinfo === '/admin/dash') {
                    return array (  '_controller' => 'moocApp\\MoocBundle\\Controller\\DefaultController::dashAction',  '_route' => 'dashboard',);
                }

                if (0 === strpos($pathinfo, '/admin/Organisme')) {
                    // valider_organisme
                    if ($pathinfo === '/admin/Organisme') {
                        return array (  '_controller' => 'moocApp\\MoocBundle\\Controller\\AdminController::afficherCompteDashOrganismeAction',  '_route' => 'valider_organisme',);
                    }

                    // valider_organismeaction
                    if (preg_match('#^/admin/Organisme/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'valider_organismeaction')), array (  '_controller' => 'moocApp\\MoocBundle\\Controller\\AdminController::validerorganismeAction',));
                    }

                }

                // supp_compte1
                if (0 === strpos($pathinfo, '/admin/supprimer') && preg_match('#^/admin/supprimer/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'supp_compte1')), array (  '_controller' => 'moocApp\\MoocBundle\\Controller\\AdminController::supprimerorganAction',));
                }

                // choix_admin_compte
                if (0 === strpos($pathinfo, '/admin/choix_admin_compte') && preg_match('#^/admin/choix_admin_compte/(?P<choix>[^/]++)/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'choix_admin_compte')), array (  '_controller' => 'moocApp\\MoocBundle\\Controller\\AdminController::choixAdminCompteAction',));
                }

                // afficher_compte_admin
                if ($pathinfo === '/admin/afficherCompte') {
                    return array (  '_controller' => 'moocApp\\MoocBundle\\Controller\\AdminController::afficherCompteDashAction',  '_route' => 'afficher_compte_admin',);
                }

                // affiche_detail_compte
                if (0 === strpos($pathinfo, '/admin/detailCompte') && preg_match('#^/admin/detailCompte/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'affiche_detail_compte')), array (  '_controller' => 'moocApp\\MoocBundle\\Controller\\AdminController::aficherDetailCompteAction',));
                }

                // rechercher_compte
                if ($pathinfo === '/admin/recherche') {
                    return array (  '_controller' => 'moocApp\\MoocBundle\\Controller\\AdminController::rechercheAction',  '_route' => 'rechercher_compte',);
                }

                // supp_compte
                if (0 === strpos($pathinfo, '/admin/supprimer') && preg_match('#^/admin/supprimer/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'supp_compte')), array (  '_controller' => 'moocApp\\MoocBundle\\Controller\\AdminController::supprimerAction',));
                }

            }

            // index_Apprenant
            if ($pathinfo === '/apprenant/home') {
                return array (  '_controller' => 'moocApp\\MoocBundle\\Controller\\DefaultController::indexAAction',  '_route' => 'index_Apprenant',);
            }

        }

        // index_Organisme
        if ($pathinfo === '/organisme/home') {
            return array (  '_controller' => 'moocApp\\MoocBundle\\Controller\\DefaultController::indexOAction',  '_route' => 'index_Organisme',);
        }

        // afficher_trad
        if ($pathinfo === '/afficher_trad') {
            return array (  '_controller' => 'moocApp\\MoocBundle\\Controller\\DefaultController::afficherAction',  '_route' => 'afficher_trad',);
        }

        // detail_client
        if ($pathinfo === '/detail') {
            return array (  '_controller' => 'moocApp\\MoocBundle\\Controller\\DefaultController::detailClientAction',  '_route' => 'detail_client',);
        }

        // modifier
        if (0 === strpos($pathinfo, '/modifier') && preg_match('#^/modifier/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'modifier')), array (  '_controller' => 'moocApp\\MoocBundle\\Controller\\DefaultController::modifierApprenantAction',));
        }

        // cher
        if ($pathinfo === '/admin/recherche') {
            return array (  '_controller' => 'moocApp\\MoocBundle\\Controller\\AdminController::rechercheAction',  '_route' => 'cher',);
        }

        // my_app_mail_succ
        if ($pathinfo === '/succ') {
            return array (  '_controller' => 'moocApp\\MoocBundle\\Controller\\MailController::indexAction',  '_route' => 'my_app_mail_succ',);
        }

        // my_app_mail_form
        if ($pathinfo === '/mail') {
            return array (  '_controller' => 'moocApp\\MoocBundle\\Controller\\MailController::newAction',  '_route' => 'my_app_mail_form',);
        }

        // my_app_mail_sendpage
        if ($pathinfo === '/sendmail') {
            return array (  '_controller' => 'moocApp\\MoocBundle\\Controller\\MailController::sendMailAction',  '_route' => 'my_app_mail_sendpage',);
        }

        // erreur
        if ($pathinfo === '/erreur') {
            return array (  '_controller' => 'moocApp\\MoocBundle\\Controller\\DefaultController::erreurAction',  '_route' => 'erreur',);
        }

        // homepage
        if ($pathinfo === '/app/example') {
            return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::indexAction',  '_route' => 'homepage',);
        }

        if (0 === strpos($pathinfo, '/log')) {
            if (0 === strpos($pathinfo, '/login')) {
                // fos_user_security_login
                if ($pathinfo === '/login') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_fos_user_security_login;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::loginAction',  '_route' => 'fos_user_security_login',);
                }
                not_fos_user_security_login:

                // fos_user_security_check
                if ($pathinfo === '/login_check') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_fos_user_security_check;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::checkAction',  '_route' => 'fos_user_security_check',);
                }
                not_fos_user_security_check:

            }

            // fos_user_security_logout
            if ($pathinfo === '/logout') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_user_security_logout;
                }

                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::logoutAction',  '_route' => 'fos_user_security_logout',);
            }
            not_fos_user_security_logout:

        }

        if (0 === strpos($pathinfo, '/profile')) {
            // fos_user_profile_show
            if (rtrim($pathinfo, '/') === '/profile') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_user_profile_show;
                }

                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'fos_user_profile_show');
                }

                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ProfileController::showAction',  '_route' => 'fos_user_profile_show',);
            }
            not_fos_user_profile_show:

            // fos_user_profile_edit
            if ($pathinfo === '/profile/edit') {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_fos_user_profile_edit;
                }

                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ProfileController::editAction',  '_route' => 'fos_user_profile_edit',);
            }
            not_fos_user_profile_edit:

        }

        if (0 === strpos($pathinfo, '/log')) {
            if (0 === strpos($pathinfo, '/login')) {
                // login
                if ($pathinfo === '/login') {
                    return array (  '_controller' => '@MoocBundle:Default:login',  '_route' => 'login',);
                }

                // login_check
                if ($pathinfo === '/login_check') {
                    return array('_route' => 'login_check');
                }

            }

            // logout
            if ($pathinfo === '/logout') {
                return array('_route' => 'logout');
            }

        }

        if (0 === strpos($pathinfo, '/re')) {
            if (0 === strpos($pathinfo, '/register')) {
                // fos_user_registration_register
                if (rtrim($pathinfo, '/') === '/register') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_fos_user_registration_register;
                    }

                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'fos_user_registration_register');
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::registerAction',  '_route' => 'fos_user_registration_register',);
                }
                not_fos_user_registration_register:

                if (0 === strpos($pathinfo, '/register/c')) {
                    // fos_user_registration_check_email
                    if ($pathinfo === '/register/check-email') {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_fos_user_registration_check_email;
                        }

                        return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::checkEmailAction',  '_route' => 'fos_user_registration_check_email',);
                    }
                    not_fos_user_registration_check_email:

                    if (0 === strpos($pathinfo, '/register/confirm')) {
                        // fos_user_registration_confirm
                        if (preg_match('#^/register/confirm/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_fos_user_registration_confirm;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_user_registration_confirm')), array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::confirmAction',));
                        }
                        not_fos_user_registration_confirm:

                        // fos_user_registration_confirmed
                        if ($pathinfo === '/register/confirmed') {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_fos_user_registration_confirmed;
                            }

                            return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::confirmedAction',  '_route' => 'fos_user_registration_confirmed',);
                        }
                        not_fos_user_registration_confirmed:

                    }

                }

            }

            if (0 === strpos($pathinfo, '/resetting')) {
                // fos_user_resetting_request
                if ($pathinfo === '/resetting/request') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_fos_user_resetting_request;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::requestAction',  '_route' => 'fos_user_resetting_request',);
                }
                not_fos_user_resetting_request:

                // fos_user_resetting_send_email
                if ($pathinfo === '/resetting/send-email') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_fos_user_resetting_send_email;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::sendEmailAction',  '_route' => 'fos_user_resetting_send_email',);
                }
                not_fos_user_resetting_send_email:

                // fos_user_resetting_check_email
                if ($pathinfo === '/resetting/check-email') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_fos_user_resetting_check_email;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::checkEmailAction',  '_route' => 'fos_user_resetting_check_email',);
                }
                not_fos_user_resetting_check_email:

                // fos_user_resetting_reset
                if (0 === strpos($pathinfo, '/resetting/reset') && preg_match('#^/resetting/reset/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_fos_user_resetting_reset;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_user_resetting_reset')), array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::resetAction',));
                }
                not_fos_user_resetting_reset:

            }

        }

        // fos_user_change_password
        if ($pathinfo === '/profile/change-password') {
            if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                goto not_fos_user_change_password;
            }

            return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ChangePasswordController::changePasswordAction',  '_route' => 'fos_user_change_password',);
        }
        not_fos_user_change_password:

        // _welcome
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', '_welcome');
            }

            return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\WelcomeController::indexAction',  '_route' => '_welcome',);
        }

        if (0 === strpos($pathinfo, '/demo')) {
            if (0 === strpos($pathinfo, '/demo/secured')) {
                if (0 === strpos($pathinfo, '/demo/secured/log')) {
                    if (0 === strpos($pathinfo, '/demo/secured/login')) {
                        // _demo_login
                        if ($pathinfo === '/demo/secured/login') {
                            return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::loginAction',  '_route' => '_demo_login',);
                        }

                        // _demo_security_check
                        if ($pathinfo === '/demo/secured/login_check') {
                            return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::securityCheckAction',  '_route' => '_demo_security_check',);
                        }

                    }

                    // _demo_logout
                    if ($pathinfo === '/demo/secured/logout') {
                        return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::logoutAction',  '_route' => '_demo_logout',);
                    }

                }

                if (0 === strpos($pathinfo, '/demo/secured/hello')) {
                    // acme_demo_secured_hello
                    if ($pathinfo === '/demo/secured/hello') {
                        return array (  'name' => 'World',  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::helloAction',  '_route' => 'acme_demo_secured_hello',);
                    }

                    // _demo_secured_hello
                    if (preg_match('#^/demo/secured/hello/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => '_demo_secured_hello')), array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::helloAction',));
                    }

                    // _demo_secured_hello_admin
                    if (0 === strpos($pathinfo, '/demo/secured/hello/admin') && preg_match('#^/demo/secured/hello/admin/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => '_demo_secured_hello_admin')), array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::helloadminAction',));
                    }

                }

            }

            // _demo
            if (rtrim($pathinfo, '/') === '/demo') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', '_demo');
                }

                return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\DemoController::indexAction',  '_route' => '_demo',);
            }

            // _demo_hello
            if (0 === strpos($pathinfo, '/demo/hello') && preg_match('#^/demo/hello/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_demo_hello')), array (  '_controller' => 'Acme\\DemoBundle\\Controller\\DemoController::helloAction',));
            }

            // _demo_contact
            if ($pathinfo === '/demo/contact') {
                return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\DemoController::contactAction',  '_route' => '_demo_contact',);
            }

        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
